#include <iostream>
#include "NB.h"
using namespace std;

int main()
{
    int quantidadeDeVariaveis = 1;
    int quantidadeDeDadosDeTreinamento = 10;
    int quantidadeDeClassesDeResposta = 2;
    int * query = new int[quantidadeDeVariaveis];

    try
    {
        //Pergunta para o primeiro exerc�cio. Sobre mau pagador.
        /*query[0] = 0;
        query[1] = 2;
        query[2] = 1;*/

        //Pergunta para o exerc�cio do T�nis.
        /*query[0] = 0;
        query[1] = 0;
        query[2] = 1;
        query[3] = 1;*/

        //Pergunta para o exerc�cio 2 (Do Telefone e Mac).
        query[0] = 1;

        NB naiveBayes(quantidadeDeVariaveis, quantidadeDeDadosDeTreinamento, quantidadeDeClassesDeResposta, query);
    }
    catch(const char* mensagemDeErro)
    {
        cout<<"Ocorreu um erro ao tentar classificar os exemplos: "<<mensagemDeErro<<endl;
    }

    return 0;
}
