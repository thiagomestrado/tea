#ifndef VARIAVELFREQUENCIA_H
#define VARIAVELFREQUENCIA_H
#include <iostream>
#include <vector>
#include "Variavel.h"

using namespace std;

class VariavelFrequencia
{
    public:
        VariavelFrequencia();
        void adicionarValor(int valor, int respostaEsperada);
        double obterValorProbabilidade(int valor, int respostaEsperada);
        int obterFrequenciaDoValor(int valor);

        //vector<vector<double>> probabilidadeDoValorPorClasse;
        vector<Variavel> probabilidadeDoValorPorClasse;
        vector<int> valores;

    protected:

    private:
        int encontrarValor(int valor, int respostaEsperada);
};

#endif // VARIAVELFREQUENCIA_H
