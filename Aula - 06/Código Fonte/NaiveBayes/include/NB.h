#ifndef NB_H
#define NB_H
#include <fstream> //Para leitura do arquivo
#include "VariavelFrequencia.h"

using namespace std;

class NB
{
    public:
        NB(int quantidadeDeVariaveis, int quantidadeDeDadosDeTreinamento, int quantidadeDeClassesDeResposta, int * query);
        void inicializarFrequencias();
        void determinarClasses();
        void obterResultado();
        void calcularProbabilidadeDaClasse();

        int quantidadeDeVariaveis;
        int quantidadeDeDadosDeTreinamento;
        int quantidadeDeClassesDeResposta;
        double * respostaPorClasse;

    protected:

    private:
        VariavelFrequencia * frequenciasDeVariaveis;
        int * query;
        int * respostasEsperadas;
        int * probabilidadePorClasse;
};

#endif // NB_H
