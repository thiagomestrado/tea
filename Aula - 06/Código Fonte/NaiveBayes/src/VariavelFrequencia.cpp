#include "VariavelFrequencia.h"

VariavelFrequencia::VariavelFrequencia()
{
    //probabilidadeDoValorPorClasse = vector<vector<double>>();
    probabilidadeDoValorPorClasse = vector<Variavel>();
    //valores = vector<int>();
}

void VariavelFrequencia::adicionarValor(int valor, int respostaEsperada)
{
    int posicaoValor = encontrarValor(valor, respostaEsperada);
    if(posicaoValor >= 0)
    {
        //cout<<"valor ja existe"<<endl;
        //probabilidadeDoValorPorClasse[valor][respostaEsperada] += 1;
        probabilidadeDoValorPorClasse[posicaoValor].quantidadeTotal += 1;
    }
    else
    {
        //valores.push_back(valor);
        probabilidadeDoValorPorClasse.push_back(Variavel(valor, respostaEsperada));
        //cout<<"Valor Adicionado: "<<valor<<endl;
    }
}

double VariavelFrequencia::obterValorProbabilidade(int valor, int respostaEsperada)
{
    int indiceParaObterProbabilidade = this->encontrarValor(valor, respostaEsperada);
    //cout<<"Valor probabilidade do valor: "<<valor<<" para resposta: "<<respostaEsperada<< "eh: "<<this->probabilidadeDoValorPorClasse[indiceParaObterProbabilidade].quantidadeTotal<<endl;
    return this->probabilidadeDoValorPorClasse[indiceParaObterProbabilidade].quantidadeTotal;
}

int VariavelFrequencia::obterFrequenciaDoValor(int valor)
{
    int frequenciaTotalDoValor = 0;
    for(int i = 0; i < this->probabilidadeDoValorPorClasse.size(); i++)
    {
        if(this->probabilidadeDoValorPorClasse[i].valor == valor)
            frequenciaTotalDoValor += this->probabilidadeDoValorPorClasse[i].quantidadeTotal;
    }

    //cout<<"Frequencia do valor: "<<valor<<" eh: "<<frequenciaTotalDoValor<<endl;
    return frequenciaTotalDoValor;
}

int VariavelFrequencia::encontrarValor(int valor, int respostaEsperada)
{
    for(int i = 0; i < this->probabilidadeDoValorPorClasse.size(); i++)
    {
        if(this->probabilidadeDoValorPorClasse[i].valor == valor &&
           this->probabilidadeDoValorPorClasse[i].classe == respostaEsperada)
            return i;
    }

    return -1;
}
