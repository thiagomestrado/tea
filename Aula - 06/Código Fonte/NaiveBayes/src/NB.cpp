#include "NB.h"

NB::NB(int quantidadeDeVariaveis, int quantidadeDeDadosDeTreinamento, int quantidadeDeClassesDeResposta, int * query)
{
    this->quantidadeDeVariaveis = quantidadeDeVariaveis;
    this->quantidadeDeDadosDeTreinamento = quantidadeDeDadosDeTreinamento;
    this->quantidadeDeClassesDeResposta = quantidadeDeClassesDeResposta;
    this->query = query;

    this->frequenciasDeVariaveis = new VariavelFrequencia[this->quantidadeDeVariaveis];
    inicializarFrequencias();

    this->respostasEsperadas = new int[this->quantidadeDeDadosDeTreinamento];
    this->respostaPorClasse = new double[this->quantidadeDeClassesDeResposta];

    //Realizar as contas e guardar os valores de probabilidade. Tabela de Frequ�ncia.
    determinarClasses();

    //Calcular a probabilidade de se ter como resposta uma determinada classe.
    calcularProbabilidadeDaClasse();

    //Obter a resposta.
    obterResultado();
}

void NB::inicializarFrequencias()
{
    for(int i = 0; i < this->quantidadeDeVariaveis; i++)
    {
        this->frequenciasDeVariaveis[i] = VariavelFrequencia();
    }
}

void NB::determinarClasses()
{
    ifstream file;
    file.open("dadosTreinamentoTelefone.txt");

    for(int i = 0; i < this->quantidadeDeDadosDeTreinamento; i++)
    {
        int respostaEsperada;
        file >> respostaEsperada;
        this->respostasEsperadas[i] = respostaEsperada;

        for(int j = 0; j < this->quantidadeDeVariaveis; j++)
        {
            int valorDaVariavel;
            file >> valorDaVariavel;
            this->frequenciasDeVariaveis[j].adicionarValor(valorDaVariavel, respostaEsperada);
        }
    }

    file.close();
}

void NB::obterResultado()
{
    for(int i = 0; i < this->quantidadeDeClassesDeResposta; i++)
    {
        double resultadoParcial = this->respostaPorClasse[i] / this->quantidadeDeDadosDeTreinamento;
        double evidencias = 1;

        for(int j = 0; j < this->quantidadeDeVariaveis; j++)
        {
            int valorDaVariavel = this->query[j];
            //i cont�m o valor da classe de resposta esperada (obt�m o valor para todas as poss�veis classes).
            resultadoParcial = resultadoParcial * ((double)this->frequenciasDeVariaveis[j].obterValorProbabilidade(valorDaVariavel, i) /
                                                   (double)this->respostaPorClasse[i]);

            //Dividir pela evid�ncia n�o � necess�rio, j� que s� queremos saber a qual classe pertence.
            /*evidencias = evidencias * ((double)this->frequenciasDeVariaveis[j].obterFrequenciaDoValor(valorDaVariavel) /
                                       (double)this->quantidadeDeDadosDeTreinamento);*/
        }

        double resultado = resultadoParcial / evidencias;

        cout<<"Resultado para classe: "<<i<<" eh: "<<resultado<<endl;
    }
}

void NB::calcularProbabilidadeDaClasse()
{
    for(int i = 0; i < this->quantidadeDeDadosDeTreinamento; i++)
    {
        int valor = this->respostasEsperadas[i];
        this->respostaPorClasse[valor] += 1;
    }

    /*for(int j = 0; j < this->quantidadeDeClassesDeResposta; j++)
    {
        //this->respostaPorClasse[j] = this->respostaPorClasse[j] / this->quantidadeDeDadosDeTreinamento;
        //cout<<"Probabilidade da classe: "<<j<<" eh: "<<respostaPorClasse[j]<<endl;
    }*/
}
