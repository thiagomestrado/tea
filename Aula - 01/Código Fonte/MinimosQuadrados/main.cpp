#include <iostream>
#include <fstream> //Para ler o arquivo .dat
#include "MinimoQuadrado.h"

using namespace std;

int main()
{
    //Referencia: https://www.andrews.edu/~calkins/math/edrm611/edrm06.htm

    int metodoEscolhido;

    cout<<"Escolha o metodo para resolucao: 1 - para Metodo Linear; 2 - para Metodo Quadratico; 3 - para Multiplas variaveis"<<endl;
    cin>>metodoEscolhido;

    //Exerc�cio 01 - Temperatura/Press�o
    /*int quantidadeDePontos = 17;
    double valoresReaisDeX[17] = {194.5, 194.3, 197.9, 198.4, 199.4, 199.9, 200.9, 201.1, 201.4, 201.3, 203.6, 204.6, 209.5, 208.6, 210.7, 211.9, 212.2};
    double valoresReaisDeY[17] = {20.79, 20.79, 22.4, 22.67, 23.15, 23.35, 23.89, 23.99, 24.02, 24.01, 25.14, 26.57, 28.49, 27.76, 29.04, 29.88, 30.06};
    */

    //Exerc�cio 02 - US Census
    /*int quantidadeDePontos = 11;
    double valoresReaisDeX[11] = {1900, 1910, 1920, 1930, 1940, 1950, 1960, 1970, 1980, 1990, 2000};
    double valoresReaisDeY[11] = {75.9950, 91.9720, 105.7110, 123.2030, 131.6690, 150.6970, 179.3230, 203.2120, 226.5050, 249.6330, 281.4220};
    */

    //Exerc�cio 03 - Livros, Pesen�as e Nota
    int quantidadeDePontos = 40;
    double valoresReaisDeX[quantidadeDePontos];
    double valoresReaisDeX2[quantidadeDePontos];
    double valoresReaisDeY[quantidadeDePontos];

    std::ifstream file;
    file.open("Books_attend_grade.dat");
    int data=0;
    for(int i = 0; i < quantidadeDePontos; i++)
    {
        for(int j = 0; j < 3; j++)
        {
            file >> data;
            if(j == 0)
                valoresReaisDeX[i] = data;
            else if(j == 1)
                valoresReaisDeX2[i] = data;
            else
                valoresReaisDeY[i] = data;
        }
    }
    file.close();

    MinimoQuadrado minimoQuadrado(quantidadeDePontos, valoresReaisDeX, valoresReaisDeY, valoresReaisDeX2);

    typedef double (MinimoQuadrado::*metodosRegressao)(double);
    metodosRegressao metodosParaCalculoResposta[] = {&MinimoQuadrado::executarRegressaoLinear,
                                                     &MinimoQuadrado::executarRegressaoQuadratica,
                                                     &MinimoQuadrado::ExecutarRegressaoComMultiplasVariaveis};
    try
    {
        //double valorParaTesteDaRegressao = 200; //Para o primeiro exerc�cio (Press�o e Temperatura)
        //double valorParaTesteDaRegressao = 2010; //Para o segundo exerc�cio (US Census).
        double valorParaTesteDaRegressao = 4; //Para o terceiro exerc�cio (M�ltiplas Vari�veis);

        double resultado = minimoQuadrado.executarRegressao((metodosParaCalculoResposta[metodoEscolhido-1]), valorParaTesteDaRegressao);

        cout<<"O resultado da regressao eh: "<<resultado<<endl;
    }
    catch(char const * mensagemDeErro)
    {
        cout<<"Nao foi possivel encontrar a solucao: "<<mensagemDeErro<<endl;
        return 0;
    }

    return 0;
}
