#include "MinimoQuadrado.h"

MinimoQuadrado::MinimoQuadrado(int quantidadeDePontos, double * valoresReaisDeX, double * valoresReaisDeY, double *valoresReaisDeX2)
{
    this->valoresReaisDeX = valoresReaisDeX;
    this->valoresReaisDeY = valoresReaisDeY;
    this->quantidadeDePontos = quantidadeDePontos;
    this->valoresReaisDeX2 = valoresReaisDeX2;
}

MinimoQuadradoResposta MinimoQuadrado::metodoLinear()
{
    double somaDeX, somaDeXQuadrado, somaDeXY, somaDeY, somaDeYQuadrado;
    somaDeX = somaDeXQuadrado = somaDeXY = somaDeY = somaDeYQuadrado = 0.0;

    //Respons�vel por fazer (simplificar) os c�lculos matriciais. Como transposta, etc.
    for(int i = 0; i < this->quantidadeDePontos; i++)
    {
        double valorRealDeXTemp = this->valoresReaisDeX[i];
        double valorRealDeYTemp = this->valoresReaisDeY[i];

        somaDeX += valorRealDeXTemp;
        somaDeXQuadrado += pow(valorRealDeXTemp, 2);

        somaDeY += valorRealDeYTemp;
        somaDeYQuadrado += pow(valorRealDeYTemp, 2);

        somaDeXY += (valorRealDeXTemp * valorRealDeYTemp);
    }

    double denominadorComum = (quantidadeDePontos * somaDeXQuadrado) - pow(somaDeX, 2);

    if(denominadorComum == 0)
    {
        throw "Erro: Matriz singular! Nao tem inversa!";
    }

    MinimoQuadradoResposta respostaRetorno;
    respostaRetorno.cruzamento = ((somaDeY * somaDeXQuadrado) - (somaDeX * somaDeXY)) / denominadorComum; //A
    respostaRetorno.inclinacao = ((quantidadeDePontos * somaDeXY) - (somaDeX * somaDeY)) / denominadorComum; //B

    return respostaRetorno;
}

MinimoQuadradoResposta MinimoQuadrado::metodoMultiplasVariaveis()
{
    double somaDeX, somaDeXQuadrado, somaDeXY, somaDeY, somaDeYQuadrado, somaDeX2, somaDeX2Quadrado, SomaDeX1X2, SomaDeX2Y;
    somaDeX = somaDeXQuadrado = somaDeXY = somaDeY = somaDeYQuadrado = somaDeX2 = somaDeX2Quadrado = SomaDeX1X2 = SomaDeX2Y = 0.0;

    //Respons�vel por fazer (simplificar) os c�lculos matriciais. Como transposta, etc.
    for(int i = 0; i < this->quantidadeDePontos; i++)
    {
        double valorRealDeXTemp = this->valoresReaisDeX[i];
        double valorRealDeYTemp = this->valoresReaisDeY[i];
        double valorRealDeX2Temp = this->valoresReaisDeX2[i];

        somaDeX += valorRealDeXTemp;
        somaDeXQuadrado += pow(valorRealDeXTemp, 2);

        somaDeY += valorRealDeYTemp;
        somaDeYQuadrado += pow(valorRealDeYTemp, 2);

        somaDeX2 += valorRealDeX2Temp;
        somaDeX2Quadrado += pow(valorRealDeX2Temp, 2);

        somaDeXY += (valorRealDeXTemp * valorRealDeYTemp);
        SomaDeX2Y += (valorRealDeX2Temp * valorRealDeYTemp);
        SomaDeX1X2 += (valorRealDeXTemp * valorRealDeX2Temp);
    }

    double matrizParaObterInversa[3][3] = {{quantidadeDePontos, somaDeX, somaDeX2},
                                           {somaDeX, somaDeXQuadrado, SomaDeX1X2},
                                           {somaDeX2, SomaDeX1X2, somaDeX2Quadrado}};

    double **matrizInversa = new double*[3];
    this->obterMatrizInversa(matrizParaObterInversa, matrizInversa);

    MinimoQuadradoResposta respostaRetorno;

    //Multiplicando a matriz inversa pela transposta de X * Y
    respostaRetorno.constante = (matrizInversa[0][0] * somaDeY) + (matrizInversa[0][1] * somaDeXY) + (matrizInversa[0][2] * SomaDeX2Y);
    respostaRetorno.cruzamento = (matrizInversa[1][0] * somaDeY) + (matrizInversa[1][1] * somaDeXY) + (matrizInversa[1][2] * SomaDeX2Y);
    respostaRetorno.inclinacao = (matrizInversa[2][0] * somaDeY) + (matrizInversa[2][1] * somaDeXY) + (matrizInversa[2][2] * SomaDeX2Y);

    return respostaRetorno;
}

void MinimoQuadrado::obterMatrizInversa(double matrizParaObterInversa[3][3], double ** matrizInversa)
{
    int i, j;
    int tamanhoDaMatriz = 3;
    double determinante = 0;
    double primeiroTermo, segundoTermo;

     //Respons�vel por obter o determinante para c�lculo da matriz inversa.
    for(i = 0; i < tamanhoDaMatriz; i++)
    {
        int auxIndex1 = (i + 1) % 3;
        int auxIndex2 = (i + 2) % 3;
        determinante += (matrizParaObterInversa[0][i] * (matrizParaObterInversa[1][auxIndex1] * matrizParaObterInversa[2][auxIndex2] -
                                                        matrizParaObterInversa[1][auxIndex2] * matrizParaObterInversa[2][auxIndex1]));
    }

    for(i = 0; i < tamanhoDaMatriz; i++)
    {
        matrizInversa[i] = new double[tamanhoDaMatriz];
        for(j = 0; j < tamanhoDaMatriz; j++)
        {
            primeiroTermo = matrizParaObterInversa[(j+1)%tamanhoDaMatriz][(i+1)%tamanhoDaMatriz] *
                            matrizParaObterInversa[(j+2)%tamanhoDaMatriz][(i+2)%tamanhoDaMatriz];

            segundoTermo = matrizParaObterInversa[(j+1)%tamanhoDaMatriz][(i+2)%tamanhoDaMatriz] *
                           matrizParaObterInversa[(j+2)%tamanhoDaMatriz][(i+1)%tamanhoDaMatriz];

            matrizInversa[i][j] = (primeiroTermo - segundoTermo) / determinante;
        }
    }
}

MinimoQuadradoResposta MinimoQuadrado::metodoQuadratico()
{
    int grauDoPolinomio = 2;
    int tamanhoVetorDasSomasDeX = 2 * grauDoPolinomio + 1;
    double valoresDeSomaDeX[tamanhoVetorDasSomasDeX]; //Esse vetor cont�m os valores das somat�rias dos valores reais de X
    double valoresDeSomaDeY[grauDoPolinomio + 1]; //Esse vetor cont�m os valores das somat�rias dos valores reais de Y.

    for(int i = 0; i < tamanhoVetorDasSomasDeX; i++)
    {
        valoresDeSomaDeX[i] = 0;
        for(int j = 0; j < this->quantidadeDePontos; j++) //Fazendo os c�lculos dos somat�rios de X
        {
            valoresDeSomaDeX[i] += pow(valoresReaisDeX[j], i); //Elevar ao grau "i" do polinomio.
        }
    }

    double matrizNormal[grauDoPolinomio + 1][grauDoPolinomio + 2];
    double vetorDeCoeficientes[grauDoPolinomio + 1]; //Esse vetor cont�m os valores finais dos coeficientes.

    for(int i = 0; i <= grauDoPolinomio; i++)
    {
        for(int j = 0; j <= grauDoPolinomio; j++)
        {
            matrizNormal[i][j] = valoresDeSomaDeX[i+j]; //Preenchendo a matriz normal com os coeficientes nas posi��es � direita.
        }
    }

    //Calculando os valores de somat�ria de Y.
    for(int i = 0; i < grauDoPolinomio + 1; i++)
    {
        for(int j = 0; j < quantidadeDePontos; j++)
        {
            valoresDeSomaDeY[i] += pow(valoresReaisDeX[j], i) * valoresReaisDeY[j];
        }
    }

    //Setando o �ltimo valor da matriz normal como o valor das somas de Y.
    for(int i=0;i<=grauDoPolinomio;i++)
    {
        matrizNormal[i][grauDoPolinomio+1] = valoresDeSomaDeY[i];
    }

    grauDoPolinomio += 1; //Porque um polinomio tem sempre o n�mero de equa��es 1 a mais do que seu verdadeiro grau.
    for(int i = 0; i < grauDoPolinomio; i++) //Come�mo da Elimina��o de Gauss
    {
        for(int c = i + 1; c < grauDoPolinomio; c++)
        {
            if(matrizNormal[i][i] < matrizNormal[c][i])
            {
                for(int j = 0; j <=grauDoPolinomio; j++)
                {
                    double auxiliar = matrizNormal[i][j];
                    matrizNormal[i][j] = matrizNormal[c][j];
                    matrizNormal[c][j] = auxiliar;
                }
            }
        }
    }


    for(int i = 0; i < grauDoPolinomio - 1; i++)
    {
        for(int c = i + 1; c < grauDoPolinomio; c++)
        {
            double auxiliar = matrizNormal[c][i] / matrizNormal[i][i];
            for(int j = 0; j <= grauDoPolinomio; j++)
            {
                matrizNormal[c][j] = matrizNormal[c][j] - (auxiliar * matrizNormal[i][j]); //De fato faz a elimina��o.

            }
        }
    }

    //Substitui��o ao contr�rio.
    for(int i = grauDoPolinomio - 1; i >= 0; i--)
    {
        vetorDeCoeficientes[i] = matrizNormal[i][grauDoPolinomio];
        for(int j = 0; j < grauDoPolinomio; j++)
        {
            if(j != i)
            {
                vetorDeCoeficientes[i] = vetorDeCoeficientes[i] - (matrizNormal[i][j] * vetorDeCoeficientes[j]);
            }
        }
        vetorDeCoeficientes[i] = vetorDeCoeficientes[i] / matrizNormal[i][i];
    }

    MinimoQuadradoResposta respostaRetorno;
    respostaRetorno.constante = vetorDeCoeficientes[0];
    respostaRetorno.cruzamento = vetorDeCoeficientes[1];
    respostaRetorno.inclinacao = vetorDeCoeficientes[2];

    return respostaRetorno;
}

double MinimoQuadrado::executarRegressao(metodosRegressao metodo, double valorParaEncontrarResposta)
{
    return (this->*metodo)(valorParaEncontrarResposta);
}

double MinimoQuadrado::executarRegressaoLinear(double valorParaEncontrarResposta)
{
    MinimoQuadradoResposta valoresDaEquacao = this->metodoLinear();

    //Equacao: Y = A + BX
    return valoresDaEquacao.cruzamento + (valoresDaEquacao.inclinacao * valorParaEncontrarResposta);
}

double MinimoQuadrado::executarRegressaoQuadratica(double valorParaEncontrar)
{
    MinimoQuadradoResposta valoresDaEquacao = this->metodoQuadratico();

    //Equa��o: Y = AX^2 + BX + C
    return (valoresDaEquacao.inclinacao * pow(valorParaEncontrar, 2)) + (valoresDaEquacao.cruzamento * valorParaEncontrar) +
            valoresDaEquacao.constante;
}

double MinimoQuadrado::ExecutarRegressaoComMultiplasVariaveis (double valorParaEncontrar)
{
    MinimoQuadradoResposta valoresDaEquacao = this->metodoMultiplasVariaveis();
    double valorParaEncontrarX2 = 20;

    //Equa��o: Y = C + A1*X1 + A2*X2
    return ((valoresDaEquacao.constante) + (valoresDaEquacao.cruzamento * valorParaEncontrar) + (valoresDaEquacao.inclinacao * valorParaEncontrarX2));
}

