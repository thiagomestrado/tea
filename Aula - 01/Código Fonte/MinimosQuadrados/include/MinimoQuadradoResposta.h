#ifndef MINIMOQUADRADORESPOSTA_H_INCLUDED
#define MINIMOQUADRADORESPOSTA_H_INCLUDED

struct MinimoQuadradoResposta
{
    double cruzamento;
    double inclinacao;
    double constante;
};

#endif // MINIMOQUADRADORESPOSTA_H_INCLUDED
