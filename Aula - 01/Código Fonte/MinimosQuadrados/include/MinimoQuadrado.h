#ifndef MINIMOQUADRADO_H
#define MINIMOQUADRADO_H
#include<iostream>
#include<math.h>
#include "MinimoQuadradoResposta.h"
using namespace std;

class MinimoQuadrado
{
    public:
        MinimoQuadrado(int quantidadeDePontos, double * valoresReaisDeX, double * valoresReaisDeY, double * valoresReaisDeX2);

        typedef double (MinimoQuadrado::*metodosRegressao)(double);
        double executarRegressao(metodosRegressao metodo, double valorParaEncontrarResposta);

        MinimoQuadradoResposta metodoLinear();
        double executarRegressaoLinear(double valorParaEncontrarResposta);

        MinimoQuadradoResposta metodoQuadratico();
        double executarRegressaoQuadratica(double valorParaEncontrarResposta);

        MinimoQuadradoResposta metodoMultiplasVariaveis();
        double ExecutarRegressaoComMultiplasVariaveis(double valorParaEncontrarResposta);

    protected:

    private:
        double * valoresReaisDeX;
        double * valoresReaisDeY;
        int quantidadeDePontos;
        double * valoresReaisDeX2;
        void obterMatrizInversa (double matrizParaObterInversa[3][3], double ** matrizInversa);
};

#endif // MINIMOQUADRADO_H
