#include <iostream>
#include <fstream> //Para ler o arquivo .dat
#include "include/PCA.h"

using namespace std;

int main()
{
    int exercicioEscolhido;

    cout<<"**SOLUCAO COM PCA**"<<endl<<endl;
    cout<<"Escolha o exercicio para solucionar: 1 - para Temperatura/Pressao; 2 - para Censo Americano; 3 - para Livro/Presenca."<<endl;
    cin>>exercicioEscolhido;

    //Exerc�cio 01 - Temperatura/Press�o
    /*int quantidadeDePontos = 17;
    double valoresReaisDeX[17] = {194.5, 194.3, 197.9, 198.4, 199.4, 199.9, 200.9, 201.1, 201.4, 201.3, 203.6, 204.6, 209.5, 208.6, 210.7, 211.9, 212.2};
    double * valoresReaisDeX2 = nullptr;
    double valoresReaisDeY[17] = {20.79, 20.79, 22.4, 22.67, 23.15, 23.35, 23.89, 23.99, 24.02, 24.01, 25.14, 26.57, 28.49, 27.76, 29.04, 29.88, 30.06};
*/
     //Exerc�cio 02 - US Census
    /*int quantidadeDePontos = 11;
    double valoresReaisDeX[11] = {1900, 1910, 1920, 1930, 1940, 1950, 1960, 1970, 1980, 1990, 2000};
    double * valoresReaisDeX2 = nullptr;
    double valoresReaisDeY[11] = {75.9950, 91.9720, 105.7110, 123.2030, 131.6690, 150.6970, 179.3230, 203.2120, 226.5050, 249.6330, 281.4220};
*/

    //Exerc�cio 03 - Livros, Pesen�as e Nota
    int quantidadeDePontos = 40;
    double valoresReaisDeX[quantidadeDePontos];
    double valoresReaisDeX2[quantidadeDePontos];
    double valoresReaisDeY[quantidadeDePontos];

    std::ifstream file;
    file.open("Books_attend_grade.dat");
    int data=0;
    for(int i = 0; i < quantidadeDePontos; i++)
    {
        for(int j = 0; j < 3; j++)
        {
            file >> data;
            if(j == 0)
                valoresReaisDeX[i] = data;
            else if(j == 1)
                valoresReaisDeX2[i] = data;
            else
                valoresReaisDeY[i] = data;
        }
    }
    file.close();

    //Para teste
    /*int quantidadeDePontos = 10;
    double valoresReaisDeX[10] = {2.5, 0.5, 2.2, 1.9, 3.1, 2.3, 2, 1, 1.5, 1.1};
    double * valoresReaisDeX2 = nullptr;
    double valoresReaisDeY[10] = {2.4, 0.7, 2.9, 2.2, 3, 2.7, 1.6, 1.1, 1.6, 0.9};
*/

    /*int quantidadeDePontos = 5;
    double valoresReaisDeX[5] = {5, 6, 2, 4, 8};
    double valoresReaisDeX2[5] = {5,4,2,1,8};
    double valoresReaisDeY[5] = {0,1,0,2,2};*/

    try
    {
        PCA pca(quantidadeDePontos, valoresReaisDeX, valoresReaisDeX2, valoresReaisDeY, 3);
        pca.executarPCA();

        /*double valorDaMedia = pca.calcularMedia(valoresReaisDeX);
        double valorDaMediaY = pca.calcularMedia(valoresReaisDeY);
        //double valorDaMediaX2 = pca.calcularMedia(valoresReaisDeX2);

        cout<<"Valor da Media de X "<<valorDaMedia<<endl;
        cout<<"Valor da Media de Y "<<valorDaMediaY<<endl;

        double subtracaoDaMediaX[quantidadeDePontos];
        double subtracaoDaMediaY[quantidadeDePontos];
        double subtracaoDaMediaX2[quantidadeDePontos];

        double var1 = pca.subtrairMedia(valoresReaisDeX, subtracaoDaMediaX, valorDaMedia);
        double var2 = pca.subtrairMedia(valoresReaisDeY, subtracaoDaMediaY, valorDaMediaY);
        //double var3 = pca.subtrairMedia(valoresReaisDeX2, subtracaoDaMediaX2, valorDaMediaX2);

        double coVariancia = pca.calcularCoVariancia(subtracaoDaMediaX, subtracaoDaMediaY);
        //double coVariancia2 = pca.calcularCoVariancia(subtracaoDaMediaX, subtracaoDaMediaX2);

        for(int i = 0; i < quantidadeDePontos; i++)
        {
            cout<<"Valores de X: "<<subtracaoDaMediaX[i]<<endl;
        }
        cout<<endl<<endl;
        for(int i = 0; i < quantidadeDePontos; i++)
        {
            cout<<"Valores de Y: "<<subtracaoDaMediaY[i]<<endl;
        }

        cout<<"VAR 1: "<<var1<<endl;
        cout<<"VAR 2: "<<var2<<endl;
        cout<<"CoVAR X e Y: "<<coVariancia<<endl;
        //cout<<"COVAR X1 e X2: "<<coVariancia2<<endl;*/
    }
    catch(char const * mensagemDeErro)
    {
        cout<<"Nao foi possivel encontrar a solucao: "<<mensagemDeErro<<endl;
        return 0;
    }

    return 0;
}
