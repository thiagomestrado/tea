#include "../include/PCA.h"

PCA::PCA(int quantidadeDePontos, double * valoresReaisDeX, double * valoresReaisDeX2, double * valoresReaisDeY, int ordemDaMatriz)
{
    //Dados iniciais dos pontos.
    this->quantidadeDePontos = quantidadeDePontos;
    this->valoresReaisDeX = valoresReaisDeX;
    this->valoresReaisDeX2 = valoresReaisDeX2;
    this->valoresReaisDeY = valoresReaisDeY;
    this->ordemDaMatriz = ordemDaMatriz;

    //Inicializando os vetores que ser�o usados para armazenar os valores dos pontos subtraindo suas m�dias.
    this->valoresDeXSubtraindoMedia = new double[quantidadeDePontos];
    this->valoresDeX2SubtraindoMedia = new double[quantidadeDePontos];
    this->valoresDeYSubtraindoMedia = new double[quantidadeDePontos];

    this->mediaDeX = calcularMedia(valoresReaisDeX);
    this->mediaDeY = calcularMedia(valoresReaisDeY);

    if(valoresReaisDeX2 != nullptr)
        this->mediaDeX2 = calcularMedia(valoresReaisDeX2);

    //Preparando a matriz de covariancia
    this->matrizDeCovariancia = new double*[quantidadeDePontos];
    for(int i = 0; i < this->quantidadeDePontos; i++)
    {
        this->matrizDeCovariancia[i] = new double[quantidadeDePontos];
    }
}

double PCA::executarPCA()
{
    double valorDaMediaX = this->calcularMedia(this->valoresReaisDeX);
    double valorDaMediaY = this->calcularMedia(this->valoresReaisDeY);
    double valorDaMediaX2;

    double varianciaDeX = this->subtrairMedia(this->valoresReaisDeX, this->valoresDeXSubtraindoMedia, valorDaMediaX);
    double varianciaDeY = this->subtrairMedia(this->valoresReaisDeY, this->valoresDeYSubtraindoMedia, valorDaMediaY);
    double varianciaDeX2;

    double coVarianciaXY = this->calcularCoVariancia(this->valoresDeXSubtraindoMedia, this->valoresDeYSubtraindoMedia);
    double coVarianciaX1X2, coVarianciaX2Y;

    double autoValor;
    double * autoVetor = new double[this->ordemDaMatriz];

    if(this->ordemDaMatriz == 2)
    {
        montarMatrizCovariancia2(varianciaDeX, varianciaDeY, coVarianciaXY);
    }
    else
    {
        valorDaMediaX2 = this->calcularMedia(this->valoresReaisDeX2);
        varianciaDeX2 = this->subtrairMedia(this->valoresReaisDeX2, this->valoresDeX2SubtraindoMedia, valorDaMediaX2);
        coVarianciaX1X2 = this->calcularCoVariancia(this->valoresDeXSubtraindoMedia, this->valoresDeX2SubtraindoMedia);
        coVarianciaX2Y = this->calcularCoVariancia(this->valoresDeX2SubtraindoMedia, this->valoresDeYSubtraindoMedia);

        montarMatrizCovariancia3(varianciaDeX, varianciaDeX2, varianciaDeX, coVarianciaXY, coVarianciaX1X2, coVarianciaX2Y);
    }

    autoValor = calcularComMetodoDaPotencia(autoVetor);

    /*for(int i = 0; i < this->ordemDaMatriz; i++)
    {
        for(int j = 0; j < this->ordemDaMatriz; j++)
        {
            cout<<"Matriz Covar: "<<this->matrizDeCovariancia[i][j]<<endl;
        }
    }*/

    /*cout<<"AUTOVALOR: "<<autoValor<<endl<<endl;
    for(int i = 0; i < this->ordemDaMatriz; i++)
    {
        cout<<"AUTOVETOR: "<<autoVetor[i]<<endl;
    }*/

    /*autoVetor[0] = -0.677873399;
    autoVetor[1] = -0.735178656;*/

    double * valoresFinaisDeX = new double[this->quantidadeDePontos];
    for(int i = 0; i < this->quantidadeDePontos; i++)
    {
        //valoresFinaisDeX[i] = autoVetor[0] * this->valoresDeXSubtraindoMedia[i] + autoVetor[1] * this->valoresDeYSubtraindoMedia[i];
        valoresFinaisDeX[i] = autoVetor[0] * this->valoresDeXSubtraindoMedia[i] + autoVetor[1] * this->valoresDeX2SubtraindoMedia[i] + autoVetor[2] * this->valoresDeYSubtraindoMedia[i];
    }

    double valoresRecuperadosDeX [this->quantidadeDePontos];
    double valoresRecuperadosDeY [this->quantidadeDePontos];
    double valoresRecuperadosDeX2 [this->quantidadeDePontos];

    for(int i = 0; i < this->quantidadeDePontos; i++)
    {
        valoresRecuperadosDeX[i] = autoVetor[0] * valoresFinaisDeX[i] + valorDaMediaX;
        valoresRecuperadosDeX2[i] = autoVetor[1] * valoresFinaisDeX[i] + valorDaMediaX2;
        valoresRecuperadosDeY[i] = autoVetor[2] * valoresFinaisDeX[i] + valorDaMediaY;

        cout<<"Valor Recuperado de X: "<<valoresRecuperadosDeX[i]<< " X2: "<<valoresRecuperadosDeX2[i]<<" e Y: "<<valoresRecuperadosDeY[i]<<endl;
    }

    return 0;
}

double PCA::calcularMedia(double * valoresParaCalcularMedia)
{
    double valorDaMedia = 0;
    for(int i = 0; i < this->quantidadeDePontos; i++)
    {
        valorDaMedia += valoresParaCalcularMedia[i];
    }

    return valorDaMedia / this->quantidadeDePontos;
}


double PCA::subtrairMedia(double * valoresParaRealizarSubtracao, double * valoresSubtraidos, double valorDaMedia)
{
    //double valorDaMedia = this->calcularMedia(valoresParaRealizarSubtracao);
    double variancia = 0;

    for(int i = 0; i < this->quantidadeDePontos; i++)
    {
        double subtraindoMediaDaEntrada = valoresParaRealizarSubtracao[i] - valorDaMedia;
        valoresSubtraidos[i] = subtraindoMediaDaEntrada;
        variancia += pow(subtraindoMediaDaEntrada, 2);
    }

    return variancia / (quantidadeDePontos - 1);
}

/*double PCA::calcularVariancia(double * valoresSubtraidos)
{
    double variancia = 0;
    for(int i = 0; i < this->quantidadeDePontos; i++)
    {
        variancia += pow(valoresSubtraidos[i], 2);
    }

    return variancia / quantidadeDePontos;
}*/

double PCA::calcularCoVariancia(double * valoresDeEntrada1, double *valoresDeEntrada2)
{
    double coVariancia = 0;
    for(int i = 0; i < this->quantidadeDePontos; i++)
    {
        coVariancia += valoresDeEntrada1[i] * valoresDeEntrada2[i];
    }

    //Faz-se por n�mero de pontos - 1 porque est� fazendo o c�lculo da m�dia a partir das observa��es, ela n�o � obtida de outro lugar.
    return coVariancia / (this->quantidadeDePontos - 1);
}

void PCA::montarMatrizCovariancia2(double varianciaX, double varianciaY, double coVarianciaXY)
{
    this->matrizDeCovariancia[0][0] = varianciaX;
    this->matrizDeCovariancia[0][1] = coVarianciaXY;
    this->matrizDeCovariancia[1][0] = coVarianciaXY;
    this->matrizDeCovariancia[1][1] = varianciaY;
}

void PCA::montarMatrizCovariancia3(double varianciaX, double varianciaX2, double varianciaY, double coVarianciaXY, double coVarianciaX1X2, double coVarianciaX2Y)
{
    this->matrizDeCovariancia[0][0] = varianciaX;
    this->matrizDeCovariancia[0][1] = coVarianciaX1X2;
    this->matrizDeCovariancia[0][2] = coVarianciaXY;

    this->matrizDeCovariancia[1][0] = coVarianciaX1X2;
    this->matrizDeCovariancia[1][1] = varianciaX2;
    this->matrizDeCovariancia[1][2] = coVarianciaX2Y;

    this->matrizDeCovariancia[2][0] = coVarianciaXY;
    this->matrizDeCovariancia[2][1] = coVarianciaX2Y;
    this->matrizDeCovariancia[2][2] = varianciaY;
}


double PCA::calcularComMetodoDaPotencia(double * autoVetor)
{
    int i, j;
    double autoVetorInicial[this->ordemDaMatriz], produtoMatrizEAutoVetorInicial[this->ordemDaMatriz], autoValorAuxiliar;
    double taxaPermitida = 0.00001;
    double autoValor = 0;

    for(i = 0; i < this->ordemDaMatriz; i++)
    {
        //Os valores iniciais do autovetor est�o normalizados
        autoVetor[i] = 1 / (double)this->ordemDaMatriz;
    }

    do
    {
        autoValorAuxiliar = autoValor;

        for(i = 0; i < this->ordemDaMatriz; i++)
        {
            produtoMatrizEAutoVetorInicial[i] = 0;
            for (j = 0; j < this->ordemDaMatriz; j++)
            {
                produtoMatrizEAutoVetorInicial[i] = produtoMatrizEAutoVetorInicial[i] + (this->matrizDeCovariancia[i][j] * autoVetor[j]);
            }
        }

        autoValor = abs(produtoMatrizEAutoVetorInicial[0]);
        for (i = 1; i < this->ordemDaMatriz; i++)
        {
            //Encontrando o maior valor, e tal valor corresponde ao autovalor.
            autoValor = abs(produtoMatrizEAutoVetorInicial[i]) > abs(autoValor) ? produtoMatrizEAutoVetorInicial[i] : autoValor;
        }

        //For respons�vel por calcular o autovetor
        for (i = 0; i < this->ordemDaMatriz; i++)
        {
            autoVetor[i] = produtoMatrizEAutoVetorInicial[i] / autoValor;
        }

    }
    while(abs(autoValor - autoValorAuxiliar) > taxaPermitida);

    return autoValor;
}
