#ifndef PCA_H
#define PCA_H

#include<iostream>
#include<cmath>

using namespace std;


class PCA
{
    public:
        PCA(int quantidadeDePontos, double * valoresReaisDeX, double * valoresReaisDeX2, double * valoresReaisDeY, int ordemDaMatriz);
        double executarPCA();
        //Retorna a vari�ncia, utilizei o mesmo m�todo para poupar repeti��o.
        double subtrairMedia(double * valoresParaRealizarSubtracao, double * valoresSubtraidos, double valorDaMedia);
        double calcularMedia(double * valoresParaCalcularMedia);
        //double calcularVariancia(double * valoresParaCalcularVariancia, double valorDaMedia)
        //Calcula a covariancia entre dois vetores. Esses vetores s�o os que sofreram a subtra��o da m�dia.
        double calcularCoVariancia(double * valoresDeEntrada1, double *valoresDeEntrada2);
        void montarMatrizCovariancia2(double varianciaX, double varianciaY, double coVarianciaXY);
        void montarMatrizCovariancia3(double varianciaX, double varianciaX2, double varianciaY, double coVarianciaXY, double coVarianciaX1X2, double coVarianciaX2Y);
        double calcularComMetodoDaPotencia(double * autoVetor);

    protected:

    private:
        //Quantidade de exemplos observados que ser�o usados para obten��o da reta.
        int quantidadeDePontos;

        //Valores reias observados.
        double * valoresReaisDeX;
        double * valoresReaisDeX2;
        double * valoresReaisDeY;


        //Valores reais observados subtraindo a m�dia.
        double * valoresDeXSubtraindoMedia;
        double * valoresDeX2SubtraindoMedia;
        double * valoresDeYSubtraindoMedia;

        //Valores de m�dia para cada vetor de valores de entrada.
        double mediaDeX;
        double mediaDeX2;
        double mediaDeY;

        double **matrizDeCovariancia;

        int ordemDaMatriz;
};

#endif // PCA_H
