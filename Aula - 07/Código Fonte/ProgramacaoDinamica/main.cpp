#include <iostream>
#include "GridWorld.h"
#include "IteracaoPolitica.h"
#include "IteracaoValor.h"

using namespace std;

int main()
{
    try
    {
        GridWorld * mundoDeBlocos = new GridWorld();
        //IteracaoPolitica * iteracaoPolitica = new IteracaoPolitica(mundoDeBlocos);
        IteracaoValor * iteracaoValor = new IteracaoValor(mundoDeBlocos);
    }
    catch(const char* mensagemDeErro)
    {
        cout<<"Ocorreu um erro ao tentar utilizar DP: "<<mensagemDeErro<<endl;
    }

    return 0;
}
