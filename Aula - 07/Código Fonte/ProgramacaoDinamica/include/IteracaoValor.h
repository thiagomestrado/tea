#ifndef ITERACAOVALOR_H
#define ITERACAOVALOR_H

#include <stdlib.h>
#include "GridWorld.h"

using namespace std;

class IteracaoValor
{
    public:
        IteracaoValor(GridWorld * mundoDeBlocos);
        void avaliacaoDoValor();
        double obterAbsoluto(double primeiraParte, double segundaParte);

    protected:

    private:
        GridWorld * mundoDeBlocos;
};

#endif // ITERACAOVALOR_H
