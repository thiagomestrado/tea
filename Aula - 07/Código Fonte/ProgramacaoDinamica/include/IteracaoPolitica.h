#ifndef ITERACAOPOLITICA_H
#define ITERACAOPOLITICA_H
#include <stdlib.h>
#include "GridWorld.h"

using namespace std;

class IteracaoPolitica
{
    public:
        IteracaoPolitica(GridWorld * mundoDeBlocos);
        void avaliacaoDePolitica();
        void melhoraDePolitica();
        double obterAbsoluto(double primeiraParte, double segundaParte);

    protected:

    private:
        GridWorld * mundoDeBlocos;
};

#endif // ITERACAOPOLITICA_H
