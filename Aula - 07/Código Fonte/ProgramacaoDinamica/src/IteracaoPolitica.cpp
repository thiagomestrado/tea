#include "IteracaoPolitica.h"

IteracaoPolitica::IteracaoPolitica(GridWorld * mundoDeBlocos)
{
    cout<<"Inicializacao de Iteracao Por Politica, com avaliacao e melhora"<<endl<<endl;

    this->mundoDeBlocos = mundoDeBlocos;

    this->avaliacaoDePolitica();
    //this->melhoraDePolitica();
}

void IteracaoPolitica::avaliacaoDePolitica()
{
    double delta = 0;
    double valorMinimoDeErro = 0.0001;
    int teste = 0;
    do
    {
        delta = 0;
        for(int i = 0; i < this->mundoDeBlocos->tamanhoMundoHorizontal; i++)
        {
            for(int j = 0; j < this->mundoDeBlocos->tamanhoMundoVertical; j++)
            {
                double valorVAux =  0;//this->mundoDeBlocos->funcaoValor[i][j];
                for(int a = 0; a < this->mundoDeBlocos->numeroDeAcoes; a++)
                {
                    int * estadoFuturo = this->mundoDeBlocos->obterEstadoFuturo(i, j, a);
                    int estadoFuturoHorizontalAux = estadoFuturo[0];
                    int estadoFuturoVerticalAux = estadoFuturo[1];

                    double recompensaAux = this->mundoDeBlocos->funcaoRecompensa[i][j][a];

                    /*valorVAux += this->mundoDeBlocos->probabilidadeDeUmaAcaoSerExecutada *
                                (recompensaAux + this->mundoDeBlocos->funcaoValor[estadoFuturoHorizontalAux][estadoFuturoVerticalAux]);
                    */

                    valorVAux += this->mundoDeBlocos->valorFuncaoTransicao[i][j][a] *
                                 (recompensaAux + this->mundoDeBlocos->funcaoValor[estadoFuturoHorizontalAux][estadoFuturoVerticalAux]);
                    //cout<<this->mundoDeBlocos->valorFuncaoTransicao[i][j][a]<<" --- "<<recompensaAux<<" -- "<<this->mundoDeBlocos->funcaoValor[estadoFuturoHorizontalAux][estadoFuturoVerticalAux]<<endl;
                }
                //cout<<endl<<endl;
                //cout<<"VALOR DE V: "<<valorVAux<<endl;
                double absoluto = this->obterAbsoluto(valorVAux, this->mundoDeBlocos->funcaoValor[i][j]);
                delta = delta > absoluto ? delta : absoluto;

                this->mundoDeBlocos->funcaoValor[i][j] = valorVAux;
                //if(i == 2 && j == 2)
                    //cout<<"VALOR DE V: "<<valorVAux<<endl;
            }
        }
        teste += 1;
    }
    while(delta > valorMinimoDeErro);

    /*for(int i = 0; i < this->mundoDeBlocos->tamanhoMundoHorizontal; i++)
    {
        for(int j = 0; j < this->mundoDeBlocos->tamanhoMundoVertical; j++)
        {
            cout<<"Estado: "<<i<<" - "<<j<<"   Valor V: "<<this->mundoDeBlocos->funcaoValor[i][j]<<endl;
        }
    }*/

    this->melhoraDePolitica();
}

void IteracaoPolitica::melhoraDePolitica()
{
    bool politicaEstavel = true;
    for(int i = 0; i < this->mundoDeBlocos->tamanhoMundoHorizontal; i++)
    {
        for(int j = 0; j < this->mundoDeBlocos->tamanhoMundoVertical; j++)
        {
            //int valorPolitica = this->mundoDeBlocos->politica[i][j];
            int melhorAcao = this->mundoDeBlocos->obterAcaoComMaiorValorDePolitica(i, j);

            //Encontrando agora a melhor a��o por "one step look-ahead"
            double maiorValor = -100000;
            int melhorAcaoFutura = 0;
            for(int a = 0; a < this->mundoDeBlocos->numeroDeAcoes; a++)
            {
                int * estadoFuturo = this->mundoDeBlocos->obterEstadoFuturo(i, j, a);
                //int estadoFuturoHorizontalAux = estadoFuturo[0];
                //int estadoFuturoVerticalAux = estadoFuturo[1];

                double maiorValorAux = this->mundoDeBlocos->valorFuncaoTransicao[i][j][a] *
                                       (this->mundoDeBlocos->funcaoRecompensa[i][j][a] + this->mundoDeBlocos->funcaoValor[estadoFuturo[0]][estadoFuturo[1]]);

                if(maiorValorAux > maiorValor)
                {
                    maiorValor = maiorValorAux;
                    melhorAcaoFutura = a;
                }

                //Atualizando o valor da pol�tica por a��o.
                this->mundoDeBlocos->valorPoliticaPorAcao[i][j][a] = maiorValorAux;
            }

            if(melhorAcaoFutura != melhorAcao)
            {
                politicaEstavel = false;
            }
            this->mundoDeBlocos->politica[i][j] = melhorAcaoFutura;
        }
    }

    if(!politicaEstavel)
    {
        this->avaliacaoDePolitica();
    }
    else
    {
        for(int i = 0; i < this->mundoDeBlocos->tamanhoMundoHorizontal; i++)
        {
            for(int j = 0; j < this->mundoDeBlocos->tamanhoMundoVertical; j++)
            {
                if((i != 0 || j != 0) && (i != 3 || j != 3))
                {
                    cout<<"Estado: "<<i<<" - "<<j<<"  e acao: "<<this->mundoDeBlocos->politica[i][j]<<endl;
                }

            }
        }
    }
}

double IteracaoPolitica::obterAbsoluto(double primeiraParte, double segundaParte)
{
    double diferenca = primeiraParte - segundaParte;
    if(diferenca < 0)
        return (diferenca * (-1));

    return diferenca;
}
