#include "IteracaoValor.h"

IteracaoValor::IteracaoValor(GridWorld * mundoDeBlocos)
{
    cout<<"Inicializacao de Iteracao de Valor!!!"<<endl<<endl;

    this->mundoDeBlocos = mundoDeBlocos;

    this->avaliacaoDoValor();
}

void IteracaoValor::avaliacaoDoValor()
{
    double delta = 0;
    double valorMinimoDeErro = 0.0001;

    do
    {
        delta = 0;
        for(int i = 0; i < this->mundoDeBlocos->tamanhoMundoHorizontal; i++)
        {
            for(int j = 0; j < this->mundoDeBlocos->tamanhoMundoVertical; j++)
            {
                double valorVAux =  -10000;
                int maiorAcao = 0;

                for(int a = 0; a < this->mundoDeBlocos->numeroDeAcoes; a++)
                {
                    int * estadoFuturo = this->mundoDeBlocos->obterEstadoFuturo(i, j, a);
                    int estadoFuturoHorizontalAux = estadoFuturo[0];
                    int estadoFuturoVerticalAux = estadoFuturo[1];

                    double recompensaAux = this->mundoDeBlocos->funcaoRecompensa[i][j][a];

                    double valorAcaoAtual = this->mundoDeBlocos->valorFuncaoTransicao[i][j][a] *
                                            (recompensaAux + this->mundoDeBlocos->funcaoValor[estadoFuturoHorizontalAux][estadoFuturoVerticalAux]);

                    if(valorAcaoAtual > valorVAux)
                    {
                        valorVAux = valorAcaoAtual;
                        maiorAcao = a;
                    }
                }

                double absoluto = this->obterAbsoluto(valorVAux, this->mundoDeBlocos->funcaoValor[i][j]);
                delta = delta > absoluto ? delta : absoluto;

                this->mundoDeBlocos->funcaoValor[i][j] = valorVAux;
                this->mundoDeBlocos->politica[i][j] = maiorAcao;
            }
        }
    }
    while(delta > valorMinimoDeErro);

    for(int i = 0; i < this->mundoDeBlocos->tamanhoMundoHorizontal; i++)
    {
        for(int j = 0; j < this->mundoDeBlocos->tamanhoMundoVertical; j++)
        {
            if((i != 0 || j != 0) && (i != 3 || j != 3))
            {
                cout<<"Estado: "<<i<<" - "<<j<<"  e acao: "<<this->mundoDeBlocos->politica[i][j]<<endl;
            }
        }
    }
}


double IteracaoValor::obterAbsoluto(double primeiraParte, double segundaParte)
{
    double diferenca = primeiraParte - segundaParte;
    if(diferenca < 0)
        return (diferenca * (-1));

    return diferenca;
}
