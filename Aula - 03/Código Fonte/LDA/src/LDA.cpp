#include "LDA.h"
#include "eigen_unsym.h"

LDA::LDA(int numeroDeClasses, int numeroDeDimensoes, double ** valoresDeEntrada, int * valoresDeSaida, int numeroDeExemplos)
{
    this->numeroDeClasses = numeroDeClasses;
    this->numeroDeDimensoes = numeroDeDimensoes;
    this->valoresDeEntrada = valoresDeEntrada;
    this->valoresDeSaida = valoresDeSaida;
    this->numeroDeExemplos = numeroDeExemplos;
    this->matrizDeDispersaoIntra = new double*[this->numeroDeDimensoes];
    this->matrizDeDispersaoEntre = new double*[this->numeroDeDimensoes];
    this->numeroDeExemplosPorClasse = new int[this->numeroDeClasses];
    this->vetorDeMediaDasMediasPorDimensao = new double[this->numeroDeDimensoes];
    this->valoresDeEntradaMenosMediaGlobal = new double*[this->numeroDeExemplos];
    this->valoresDeEntradaMenosMediaGlobalTrans = new double*[this->numeroDeDimensoes];
    this->mediaGlobalPorDimensao = new double[this->numeroDeDimensoes];
    this->numeroDeDimensoesNovo = this->numeroDeClasses - 1;

    //Preparando a matriz com as m�dias por classe.
    this->mediaDasDimensoesPorClasse = new double*[this->numeroDeClasses];
    for(int i = 0; i < this->numeroDeClasses; i++)
    {
        this->mediaDasDimensoesPorClasse[i] = new double[this->numeroDeDimensoes];
    }

    //Preparando as matrizes de dispers�o para as opera��es entre classes e intra classe.
    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        this->matrizDeDispersaoIntra[i] = new double[this->numeroDeDimensoes];
        this->matrizDeDispersaoEntre[i] = new double[this->numeroDeDimensoes];

        //Zerando as matrizes de dispers�o para valores iniciais.
        for(int j = 0; j < this->numeroDeDimensoes; j++)
        {
            this->matrizDeDispersaoIntra[i][j] = 0;
            this->matrizDeDispersaoEntre[i][j] = 0;
        }
    }

    this->calcularMediaDasDimensoes();
    this->calcularMediaDasMediasPorDimensao();

    for(int a = 0; a < this->numeroDeClasses; a++)
    {
        for(int i = 0; i < this->numeroDeExemplos; i++)
        {
            if(this->valoresDeSaida[i] == (a + 1))
            {
                this->valoresDeEntradaMenosMediaGlobal[i] = new double[this->numeroDeDimensoes];
                for(int j = 0; j < this->numeroDeDimensoes; j++)
                {
                    //this->valoresDeEntradaMenosMediaGlobal[i][j] = this->valoresDeEntrada[i][j] - this->vetorDeMediaDasMediasPorDimensao[j];
                    this->valoresDeEntradaMenosMediaGlobal[i][j] = this->valoresDeEntrada[i][j] - this->mediaDasDimensoesPorClasse[a][j];
                }
            }
        }
    }

    //Transposta
    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        this->valoresDeEntradaMenosMediaGlobalTrans[i] = new double[this->numeroDeExemplos];
        for(int j = 0; j < this->numeroDeExemplos; j++)
        {
            this->valoresDeEntradaMenosMediaGlobalTrans[i][j] = this->valoresDeEntradaMenosMediaGlobal[j][i];
        }
    }

    this->calcularMatrizDeDispersaoIntra();

    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        this->mediaGlobalPorDimensao[i] = 0;
        for(int j = 0; j < this->numeroDeExemplos; j++)
        {
            this->mediaGlobalPorDimensao[i] += this->valoresDeEntrada[j][i];
        }

        this->mediaGlobalPorDimensao[i] = this->mediaGlobalPorDimensao[i] / this->numeroDeExemplos;
    }

    this->valoresDaMediaMenosMediaGlobalPorClasse = new double*[this->numeroDeClasses];
    for(int i = 0; i < this->numeroDeClasses; i++)
    {
        this->valoresDaMediaMenosMediaGlobalPorClasse[i] = new double[this->numeroDeDimensoes];
        for(int j = 0; j < this->numeroDeDimensoes; j++)
        {
            this->valoresDaMediaMenosMediaGlobalPorClasse[i][j] = this->mediaDasDimensoesPorClasse[i][j] - mediaGlobalPorDimensao[j];
        }
    }

    this->calcularMatrizDeDispersaoEntre();
}

void LDA::calcularMediaDasDimensoes()
{
    for(int i = 0; i < this->numeroDeClasses; i++)
    {
        for(int j = 0; j < this->numeroDeDimensoes; j++)
        {
            double mediaPorDimensao = 0;
            int quantidadeDeExemplosDaClasse = 0;
            for(int a = 0; a < this->numeroDeExemplos; a++)
            {
                if(this->valoresDeSaida[a] == (i+1)) //Ent�o � um exemplo dessa classe. Usar o exemplo para a m�dia.
                {
                    mediaPorDimensao += this->valoresDeEntrada[a][j];
                    quantidadeDeExemplosDaClasse += 1;
                }
            }

            mediaPorDimensao = mediaPorDimensao / quantidadeDeExemplosDaClasse;
            this->numeroDeExemplosPorClasse[i] = quantidadeDeExemplosDaClasse;
            this->mediaDasDimensoesPorClasse[i][j] = mediaPorDimensao;
            cout<<this->mediaDasDimensoesPorClasse[i][j]<<" ";
        }
        cout<<endl;
    }
}

void LDA::calcularMediaDasMediasPorDimensao()
{
    cout<<"Media das Medias: ";
    for(int i = 0; i < numeroDeDimensoes; i++)
    {
        double mediaDasMedias = 0;
        for(int j = 0; j < numeroDeClasses; j++)
        {
            mediaDasMedias += this->mediaDasDimensoesPorClasse[j][i];
        }

        mediaDasMedias = mediaDasMedias / this->numeroDeClasses;
        cout<<mediaDasMedias<<" ";
        this->vetorDeMediaDasMediasPorDimensao[i] = mediaDasMedias;
    }
    cout<<endl;
}

void LDA::calcularMatrizDeDispersaoIntra()
{
    double * diagonalPrincipal = new double[this->numeroDeDimensoes];
    //Rever essa inicializa��o do vetor de coVariancias, estou em d�vida se pra todas as matrizes esse tamanho serve.
    int numeroDeCoVariancias = this->numeroDeDimensoes + (this->numeroDeDimensoes/2);
    double * coVariancias = new double[numeroDeCoVariancias];

    calcularDiagonalPrincipalVariancia(diagonalPrincipal);
    calcularCoVariancia(coVariancias);

    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        this->matrizDeDispersaoIntra[i][i] = diagonalPrincipal[i];
    }

    int contadorAux = 0;
    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        for(int j = i+1; j < this->numeroDeDimensoes; j++)
        {
            this->matrizDeDispersaoIntra[i][j] = coVariancias[contadorAux];
            this->matrizDeDispersaoIntra[j][i] = coVariancias[contadorAux];
            contadorAux += 1;
        }
    }
}

void LDA::calcularDiagonalPrincipalVariancia(double * diagonalPrincipal)
{
    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        diagonalPrincipal[i] = 0;
        for(int j = 0; j < this->numeroDeExemplos; j++)
        {
            diagonalPrincipal[i] += pow(this->valoresDeEntradaMenosMediaGlobal[j][i], 2);
        }
    }
}

void LDA::calcularCoVariancia(double * coVariancias)
{
    int contadorAux = 0;

    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        for(int j = (i+1); j < this->numeroDeDimensoes; j++)
        {
            coVariancias[contadorAux] = 0;
            for(int a = 0; a < this->numeroDeExemplos; a++)
            {
                coVariancias[contadorAux] += this->valoresDeEntradaMenosMediaGlobal[a][i] * this->valoresDeEntradaMenosMediaGlobal[a][j];
            }

            contadorAux += 1;
        }
    }
}

void LDA::calcularMatrizDeDispersaoEntre()
{
    double * diagonalPrincipal = new double[this->numeroDeDimensoes];
    //Rever essa inicializa��o do vetor de coVariancias, estou em d�vida se pra todas as matrizes esse tamanho serve.
    int numeroDeCoVariancias = this->numeroDeDimensoes + (this->numeroDeDimensoes/2);
    double * coVariancias = new double[numeroDeCoVariancias];

    calcularDiagonalPrincipalEntreClasse(diagonalPrincipal);
    calcularCoVarianciaEntreClasse(coVariancias);

    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        this->matrizDeDispersaoEntre[i][i] = diagonalPrincipal[i];
    }

    int contadorAux = 0;
    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        for(int j = i+1; j < this->numeroDeDimensoes; j++)
        {
            this->matrizDeDispersaoEntre[i][j] = coVariancias[contadorAux];
            this->matrizDeDispersaoEntre[j][i] = coVariancias[contadorAux];
            contadorAux += 1;
        }
    }

    double ** matrizInversa = new double*[this->numeroDeDimensoes];
    double **matrizComAMultiplicacao = new double*[this->numeroDeDimensoes];
    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        matrizInversa[i] = new double[this->numeroDeDimensoes];
        matrizComAMultiplicacao[i] = new double[this->numeroDeDimensoes];
        for(int j = 0; j < this->numeroDeDimensoes; j++)
        {
            matrizComAMultiplicacao[i][j] = 0;
        }
    }

    this->inversoDaMatriz(matrizInversa);

    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        for(int j = 0; j < this->numeroDeDimensoes; j++)
        {
            for(int a = 0; a < this->numeroDeDimensoes; a++)
            {
                matrizComAMultiplicacao[i][j] += matrizInversa[i][a] * this->matrizDeDispersaoEntre[a][j];
            }
        }
    }

    //Agora encontrar AutoValores e AutoVetores

    contadorAux = 0;
    double matrizMultiplicacaoVetorizada[this->numeroDeDimensoes*this->numeroDeDimensoes];
    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        for(int j = 0; j < this->numeroDeDimensoes; j++)
        {
            matrizMultiplicacaoVetorizada[contadorAux] = matrizComAMultiplicacao[i][j];
            contadorAux += 1;
        }
    }

    MatDoub matrizParaObterEigenValues(this->numeroDeDimensoes, this->numeroDeDimensoes, matrizMultiplicacaoVetorizada);

    //Obtendo os autovalores e autovetores.
    Unsymmeig contemEigenValues(matrizParaObterEigenValues, true, true);

    double valoresRecuperados [this->numeroDeExemplos][this->numeroDeDimensoesNovo];
    for(int i = 0; i < this->numeroDeExemplos; i++)
    {
        for(int j = 0; j < this->numeroDeDimensoesNovo; j++)
        {
            valoresRecuperados[i][j] = 0;
            for(int a = 0; a < this->numeroDeDimensoes; a++)
            {
                valoresRecuperados[i][j] += this->valoresDeEntrada[i][a] * contemEigenValues.zz[a][j];
            }
        }
    }

    ofstream escreverNoArquivo, escreverNoArquivo2;
    escreverNoArquivo.open ("respostas.dat");
    escreverNoArquivo2.open("respostas2.dat");
    for(int i = 0; i < this->numeroDeExemplos; i++)
    {
        escreverNoArquivo << valoresRecuperados[i][0] <<"\n";
        escreverNoArquivo2 <<valoresRecuperados[i][1]<<"\n";
    }
    escreverNoArquivo.close();
    escreverNoArquivo2.close();
}

void LDA::calcularDiagonalPrincipalEntreClasse(double * diagonalPrincipal)
{
    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        diagonalPrincipal[i] = 0;
        for(int j = 0; j < this->numeroDeClasses; j++)
        {
            diagonalPrincipal[i] += this->numeroDeExemplosPorClasse[j] * pow(this->valoresDaMediaMenosMediaGlobalPorClasse[j][i], 2);
        }
    }
}

void LDA::calcularCoVarianciaEntreClasse(double * coVariancias)
{
    int contadorAux = 0;

    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        for(int j = (i+1); j < this->numeroDeDimensoes; j++)
        {
            coVariancias[contadorAux] = 0;
            for(int a = 0; a < this->numeroDeClasses; a++)
            {
                coVariancias[contadorAux] += this->numeroDeExemplosPorClasse[a] * this->valoresDaMediaMenosMediaGlobalPorClasse[a][i] * this->valoresDaMediaMenosMediaGlobalPorClasse[a][j];
            }

            contadorAux += 1;
        }
    }
}

void LDA::inversoDaMatriz(double ** matrizInversa)
{
    int determinante = encontrarDeterminante(this->matrizDeDispersaoIntra, this->numeroDeDimensoes);

    if(determinante == 0)
    {
        cout<<"Matriz Singular!!"<<endl;
        return;
    }

    double **matrizAdjunta;
    matrizAdjunta = new double*[this->numeroDeDimensoes];
    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        matrizAdjunta[i] = new double[this->numeroDeDimensoes];
    }
    obterMatrizAdjunta(this->matrizDeDispersaoIntra, matrizAdjunta);

    // Encontrar a matriz inversa utilizando a f�rmula: Inversa = Adjunta/Determinante"
    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        for(int j = 0; j < this->numeroDeDimensoes; j++)
        {
           matrizInversa[i][j] = matrizAdjunta[i][j] / determinante;
           //cout<<matrizInversa[i][j]<<" ";
        }

        //cout<<endl;
    }
}

double LDA::encontrarDeterminante(double ** matrizParaOperacao, int numeroDaDimensaoAtual)
{
    double determinante = 0;

    if (numeroDaDimensaoAtual == 1)
        return matrizParaOperacao[0][0];

    double **coFatoresAux = new double*[this->numeroDeDimensoes];
    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        coFatoresAux[i] = new double [this->numeroDeDimensoes];
    }

    //Sinal do multiplicador
    int sinalMultiplicador = 1;

    for (int i = 0; i < numeroDaDimensaoAtual; i++)
    {
        obterCoFator(matrizParaOperacao, coFatoresAux, 0, i, numeroDaDimensaoAtual);
        determinante += sinalMultiplicador * matrizParaOperacao[0][i] * encontrarDeterminante(coFatoresAux, numeroDaDimensaoAtual - 1);

        //Termos devem ser adicionados alternando o sinal
        sinalMultiplicador = -sinalMultiplicador;
    }

    return determinante;
}

void LDA::obterCoFator(double **matrizParaOperacao, double ** coFatoresAux, int primeiroIndice, int iteracaoAtual, int numeroDaDimensaoAtual)
{
    int contAuxLinha = 0, contAuxColuna = 0;
    //Passando por cada elemento da matriz
    for (int i = 0; i < numeroDaDimensaoAtual; i++)
    {
        for (int j = 0; j < numeroDaDimensaoAtual; j++)
        {
            if (i != primeiroIndice && j != iteracaoAtual)
            {
                coFatoresAux[contAuxLinha][contAuxColuna++] = matrizParaOperacao[i][j];

                if (contAuxColuna == numeroDaDimensaoAtual - 1)
                {
                    contAuxColuna = 0;
                    contAuxLinha++;
                }
            }
        }
    }
}

void LDA::obterMatrizAdjunta(double **matrizParaOperacao, double ** matrizAdjunta)
{
    if (this->numeroDeDimensoes == 1)
    {
        matrizAdjunta[0][0] = 1;
        return;
    }

    int sinalMultiplicador = 1;
    double **coFatoresAux = new double*[this->numeroDeDimensoes];
    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        coFatoresAux[i] = new double[this->numeroDeDimensoes];
    }

    for (int i=0; i < this->numeroDeDimensoes; i++)
    {
        for (int j=0; j < this->numeroDeDimensoes; j++)
        {
            //Obter coFator para posi��o [i][j]
            obterCoFator(matrizParaOperacao, coFatoresAux, i, j, this->numeroDeDimensoes);

            sinalMultiplicador = ((i + j) % 2 == 0) ? 1 : -1;

            matrizAdjunta[j][i] = sinalMultiplicador * encontrarDeterminante(coFatoresAux, this->numeroDeDimensoes - 1);
        }
    }
}
