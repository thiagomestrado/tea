#ifndef LDA_H
#define LDA_H
#include <iostream>
#include <cmath>
#include <fstream>

using namespace std;

class LDA
{
    public:
        LDA(int numeroDeClasses, int numeroDeDimensoes, double ** valoresDeEntrada, int * valoresDeSaida, int numeroDeExemplos);
        void calcularMediaDasDimensoes();
        void calcularMatrizDeDispersaoIntra();
        void calcularMatrizDeDispersaoEntre();
        void calcularMediaDasMediasPorDimensao();
        void calcularDiagonalPrincipalVariancia(double * diagonalPrincipal);
        void calcularCoVariancia(double * coVariancias);
        void montarMatrizDeCovariancia(double * diagonalPrincipal, double * coVariancias);
        void calcularDiagonalPrincipalEntreClasse(double * diagonalPrincipal);
        void calcularCoVarianciaEntreClasse(double * coVariancias);
        void inversoDaMatriz(double ** matrizInversa);
        double encontrarDeterminante(double ** matrizParaOperacao, int numeroDaDimensaoAtual);
        void obterCoFator(double **matrizParaOperacao, double ** coFatoresAux, int primeiroIndice, int iteracaoAtual, int numeroDaDimensaoAtual);
        void obterMatrizAdjunta(double **matrizParaOperacao, double ** matrizAdjunta);

    protected:

    private:
        int numeroDeClasses;
        int numeroDeDimensoes;
        double ** mediaDasDimensoesPorClasse;
        double ** valoresDeEntrada;
        int * valoresDeSaida;
        int numeroDeExemplos;
        double ** matrizDeDispersaoIntra;
        double ** matrizDeDispersaoEntre;
        int * numeroDeExemplosPorClasse;
        double * vetorDeMediaDasMediasPorDimensao;
        double ** valoresDeEntradaMenosMediaGlobal;
        double ** valoresDeEntradaMenosMediaGlobalTrans;
        double ** valoresDaMediaMenosMediaGlobalPorClasse;
        double * mediaGlobalPorDimensao;
        double mediaGlobal;
        int numeroDeDimensoesNovo;
};

#endif // LDA_H
