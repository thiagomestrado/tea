#include <iostream>
#include <fstream> //Para ler o arquivo .dat
#include <string>
#include "LDA.h"

using namespace std;


int obterTipoDeFlorPorNome(string nomeDoTipo)
{
    cout<<nomeDoTipo<<endl;

    if(nomeDoTipo.compare("Iris-setosa"))
        return 1;
    else if(nomeDoTipo.compare("Iris-versicolor"))
        return 2;
    else //Se for Iris-virginica
        return 3;
}

int main()
{
    int quantidadeDePontos = 150;
    double tamanhoSepal[quantidadeDePontos];
    double larguraSepal[quantidadeDePontos];
    double tamanhoPetal[quantidadeDePontos];
    double larguraPetal[quantidadeDePontos];
    int tipoDeFlor[quantidadeDePontos];

    std::ifstream file;
    file.open("iris.dat");
    double data=0.0;
    string nomeFlor;
    int numeroDeDimensoes = 4;
    int numeroDeClasses = 3;
    double ** valoresDeEntradaJuntos = new double*[quantidadeDePontos];
    for(int i = 0; i < quantidadeDePontos; i++)
    {
        valoresDeEntradaJuntos[i] = new double[numeroDeDimensoes];
        for(int j = 0; j < numeroDeDimensoes + 1; j++)
        {
            if(j == 4)
            {
                file >> tipoDeFlor[i];
            }
            else
            {
                file >> valoresDeEntradaJuntos[i][j];
            }
        }
    }
    file.close();

    try
    {
        LDA lda(numeroDeClasses, numeroDeDimensoes, valoresDeEntradaJuntos, tipoDeFlor, quantidadeDePontos);
    }
    catch(const char* mensagemDeErro)
    {
        cout<<"Ocorreu um erro ao tentar classificar os exemplos: "<<mensagemDeErro<<endl;
    }

    return 0;
}
