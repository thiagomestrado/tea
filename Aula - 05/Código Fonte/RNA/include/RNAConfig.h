#ifndef RNACONFIG_H
#define RNACONFIG_H
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include "Camada.h"

using namespace std;

class RNAConfig
{
    public:
        RNAConfig(int numeroNosCamadaEntrada, int numeroDeCamadasEscondidas, int numeroNeuroniosEscondidos, int numeroNeuroniosSaida,
                  int numeroDeDadosDeTreinamento, double taxaDeAprendizado, int numeroDeExecucacaoDeTreinamento,
                  int numeroDeDadosDeValidacao);
        double funcaoSigmoide(double valorNet);
        void validar();

    protected:

    private:
        void inicializarCamadaEntrada();
        void inicializarCamadaEscondida();
        void inicializarCamadaSaida();
        void feedForwardTreinamento();
        void feedForward();
        void backPropagation(double valorEsperado);
        int * obterValorEsperadoBinario(double valorEsperado);
        bool determinarAcertoDaRNA(int * valorEsperado, double * valorRealObtido, int numeroDeNeuronios);

        int numeroNosCamadaEntrada;
        int numeroDeCamadasEscondidas;
        int numeroNeuroniosEscondidos;
        int numeroNeuroniosSaida;
        int numeroTotalDeCamadas;
        int numeroDeDadosDeTreinamento;
        double taxaDeAprendizado;
        int numeroDeExecucacaoDeTreinamento;
        int numeroDeDadosDeValidacao;

        Camada ** camadas;
};

#endif // RNACONFIG_H
