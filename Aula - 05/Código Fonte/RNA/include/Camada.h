#ifndef CAMADA_H
#define CAMADA_H
#include <iostream>
#include <stdlib.h> //Para os n�meros rand�micos.
#include <time.h> //Usado para o seed do n�mero rand�mico.
#include <fstream> //Para leitura do arquivo

using namespace std;

class Camada
{
    public:
        Camada(int tipoDeCamada, int posicaoNaEstrutura, int numeroDeNeuronios, int numeroDeNeuroniosAnterior);
        void inicializarBias();
        void inicializarPesos(int posicaoDoNeuronio);
        void inicializarCamadaDeEntrada();

        int tipoDeCamada; //1 para Entrada, 2 para Escondida, 3 para Sa�da
        int posicaoNaEstrutura;
        int numeroDeNeuronios;
        int numeroDeNeuroniosCamadaAnterior;
        double * valoresDeAtivacao;
        double * bias;
        double ** pesos;
        double * errosAux;

    protected:

    private:
};

#endif // CAMADA_H
