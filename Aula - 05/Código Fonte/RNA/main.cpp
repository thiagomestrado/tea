#include <iostream>
#include "include/RNAConfig.h"

using namespace std;

int main()
{
    //Determinando a configuração inicial da RNA
    int numeroDeNosCamadaEntrada = 256;
    int numeroDeCamadasEscondida = 2;
    int numeroDeNeuroniosCamadaEscondida = 20;
    int numeroDeNeuroniosCamadaSaida = 2;
    int numeroDeDadosDeTreinamento = 7291;
    double taxaDeAprendizado = 0.2;
    int numeroDeExecucacaoDeTreinamento = 100;
    int numeroDeDadosDeValidacao = 2007;

    try
    {
        RNAConfig configuracaoRNA(numeroDeNosCamadaEntrada, numeroDeCamadasEscondida, numeroDeNeuroniosCamadaEscondida,
                                  numeroDeNeuroniosCamadaSaida, numeroDeDadosDeTreinamento, taxaDeAprendizado,
                                  numeroDeExecucacaoDeTreinamento, numeroDeDadosDeValidacao);
    }
    catch(const char* mensagemDeErro)
    {
        cout<<"Ocorreu um erro ao tentar classificar os exemplos: "<<mensagemDeErro<<endl;
    }

    return 0;
}
