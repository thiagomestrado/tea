#include "../include/Camada.h"

Camada::Camada(int tipoDeCamada, int posicaoNaEstrutura, int numeroDeNeuronios, int numeroDeNeuroniosAnterior)
{
    this->tipoDeCamada = tipoDeCamada;
    this->posicaoNaEstrutura = posicaoNaEstrutura;
    this->numeroDeNeuronios = numeroDeNeuronios;
    this->numeroDeNeuroniosCamadaAnterior = numeroDeNeuroniosAnterior;

    this->valoresDeAtivacao = new double[this->numeroDeNeuronios];
    this->errosAux = new double[this->numeroDeNeuronios];

    if(this->tipoDeCamada == 1) //J� inicializar com a leitura do arquivo com os valores
    {
        inicializarCamadaDeEntrada();
    }
    else //Se n�o for a camada de entrada, ent�o vai inicializar Bias e Pesos, que s� � as camadas escondidas e de sa�da t�m.
    {
        srand (time(NULL));

        this->bias = new double[this->numeroDeNeuronios];
        inicializarBias();

        this->pesos = new double*[this->numeroDeNeuronios];
        for(int i = 0; i < this->numeroDeNeuronios; i++)
        {
            this->pesos[i] = new double[this->numeroDeNeuroniosCamadaAnterior];
            inicializarPesos(i);
        }
    }

    //cout<<"Tipo de Camada: "<<tipoDeCamada<<" , quantidade de neuronios: "<<this->numeroDeNeuronios<<" , neuronios camada anterior: "<<this->numeroDeNeuroniosCamadaAnterior<<endl<<endl;
}

void Camada::inicializarBias()
{
    //Eu poderia usar o for loop que inicializa os pesos e receber apenas a posi��o para setar o bias, diminuiria a ordem do algoritmo.
    for(int i = 0; i < this->numeroDeNeuronios; i++)
    {
        this->bias[i] = (rand() % 1000) / 1000.0; //Gerando um valor de 0 at� 1(n�o incluso) com at� 3 casas decimais.
        //cout<<bias[i]<<endl;
    }
}

void Camada::inicializarPesos(int posicaoDoNeuronio)
{
    for(int i = 0; i < this->numeroDeNeuroniosCamadaAnterior; i++)
    {
        this->pesos[posicaoDoNeuronio][i] = (rand() % 1000) / 1000.0;
        //cout<<this->pesos[posicaoDoNeuronio][i]<<endl;
    }
}

void Camada::inicializarCamadaDeEntrada()
{
}
