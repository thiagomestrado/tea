#include "../include/RNAConfig.h"

RNAConfig::RNAConfig(int numeroNosCamadaEntrada, int numeroDeCamadasEscondidas, int numeroNeuroniosEscondidos, int numeroNeuroniosSaida,
                     int numeroDeDadosDeTreinamento, double taxaDeAprendizado, int numeroDeExecucacaoDeTreinamento,
                     int numeroDeDadosDeValidacao)
{
    this->numeroNosCamadaEntrada = numeroNosCamadaEntrada;
    this->numeroDeCamadasEscondidas = numeroDeCamadasEscondidas;
    this->numeroNeuroniosEscondidos = numeroNeuroniosEscondidos;
    this->numeroNeuroniosSaida = numeroNeuroniosSaida;
    this->numeroDeDadosDeTreinamento = numeroDeDadosDeTreinamento;
    this->taxaDeAprendizado = taxaDeAprendizado;
    this->numeroDeExecucacaoDeTreinamento = numeroDeExecucacaoDeTreinamento;
    this->numeroDeDadosDeValidacao = numeroDeDadosDeValidacao;

    this->numeroTotalDeCamadas = 1 + this->numeroDeCamadasEscondidas + 1;
    this->camadas = new Camada*[this->numeroTotalDeCamadas];

    //Inicializa��o da arquitetura.
    this->inicializarCamadaEntrada();
    this->inicializarCamadaEscondida();
    this->inicializarCamadaSaida();

    //Rodar Feed Forward.
    this->feedForwardTreinamento();

    //Validar RNA
    this->validar();
}

void RNAConfig::inicializarCamadaEntrada()
{
    this->camadas[0] = new Camada(1, 0, this->numeroNosCamadaEntrada, 0);
}

void RNAConfig::inicializarCamadaEscondida()
{
    this->camadas[1] = new Camada(2, 1, this->numeroNeuroniosEscondidos, this->numeroNosCamadaEntrada);

    //Come�a do 2 porque a primeira parte da camada escondida j� foi feita e tamb�m a camada de entrada.
    for(int i = 2; i <= this->numeroDeCamadasEscondidas; i++)
    {
        int numeroDeNeuroniosCamadaAnterior = this->camadas[i-1]->numeroDeNeuronios;
        this->camadas[i] = new Camada(2, i, this->numeroNeuroniosEscondidos, numeroDeNeuroniosCamadaAnterior);
    }
}

void RNAConfig::inicializarCamadaSaida()
{
    int posicaoCamadaSaida = this->numeroTotalDeCamadas - 1;
    int numeroDeNeuroniosCamadaAnterior = this->camadas[posicaoCamadaSaida - 1]->numeroDeNeuronios;

    this->camadas[posicaoCamadaSaida] = new Camada(3, posicaoCamadaSaida, this->numeroNeuroniosSaida,
                                                   numeroDeNeuroniosCamadaAnterior);
}

void RNAConfig::feedForwardTreinamento()
{
    for(int j = 0; j < this->numeroDeExecucacaoDeTreinamento; j++)
    {
        ifstream file;
        file.open("treinamento.txt");

        for(int i = 0; i < this->numeroDeDadosDeTreinamento; i++)
        {
            double valorEsperado = 0;
            file >> valorEsperado;

            for(int j = 0; j < this->numeroNosCamadaEntrada; j++)
            {
                file >> this->camadas[0]->valoresDeAtivacao[j];
            }

            if(valorEsperado == 0 || valorEsperado == 1 || valorEsperado == 2)
            {
                feedForward();
                backPropagation(valorEsperado);

                /*cout<<valorEsperado<<endl;
                int valor = 0;
                cin>>valor;*/
                /*int * valorEsperadoConvertido = obterValorEsperadoBinario(valorEsperado);
                for(int j = 0; j < 3; j++)
                {
                    cout<<this->camadas[3]->valoresDeAtivacao[j]<<" "<<valorEsperadoConvertido[j]<<" ---- ";
                }
                cout<<endl;*/

                //cin>>valorEsperado;
            }
        }

        file.close();
    }
}

void RNAConfig::feedForward()
{
    int numeroDeInteracoes = this->numeroTotalDeCamadas - 1;
    for(int i = 0; i < numeroDeInteracoes; i++)
    {
        int camadaVizinha = i + 1;
        for(int neuronioAtual = 0; neuronioAtual < this->camadas[camadaVizinha]->numeroDeNeuronios; neuronioAtual++)
        {
            double valorDeAtivacaoAux = 0;

            int numeroNeuroniosAtual = this->camadas[i]->numeroDeNeuronios;
            for(int j = 0; j < numeroNeuroniosAtual; j++)
            {
                valorDeAtivacaoAux += this->camadas[i]->valoresDeAtivacao[j] * this->camadas[camadaVizinha]->pesos[neuronioAtual][j];
            }

            valorDeAtivacaoAux += this->camadas[camadaVizinha]->bias[neuronioAtual];

            this->camadas[camadaVizinha]->valoresDeAtivacao[neuronioAtual] = this->funcaoSigmoide(valorDeAtivacaoAux);
        }
    }
}

double RNAConfig::funcaoSigmoide(double valorNet)
{
    return 1 / (1 + exp(-valorNet));
}

void RNAConfig::backPropagation(double valorEsperado)
{
    int * valorEsperadoConvertido = obterValorEsperadoBinario(valorEsperado);

    int camadaAtual = this->numeroTotalDeCamadas - 1; //Come�ando pela camada de sa�da.

    //Camada de Sa�da
    for(int i = 0; i < this->numeroNeuroniosSaida; i++)
    {
        double valorRealDeSaida = this->camadas[camadaAtual]->valoresDeAtivacao[i];

        //Em = (Tm - Sm) * f'(NETm). Esse valor � usado para ajustar o peso.
        double erroAux = ((double)valorEsperadoConvertido[i] - valorRealDeSaida) * valorRealDeSaida * (1 - valorRealDeSaida);
        this->camadas[camadaAtual]->errosAux[i] = erroAux;

        for(int j = 0; j < this->camadas[camadaAtual]->numeroDeNeuroniosCamadaAnterior; j++)
        {
            //Atualiza��o dos valores de peso.
            //Peso = taxaDeAprendizado * erroAux * valor de ativa��o do neur�nio da camada anterior.
            this->camadas[camadaAtual]->pesos[i][j] = this->camadas[camadaAtual]->pesos[i][j] +
                                                      (this->taxaDeAprendizado * erroAux * this->camadas[camadaAtual - 1]->valoresDeAtivacao[j]);
        }

        this->camadas[camadaAtual]->bias[i] = this->camadas[camadaAtual]->bias[i] + (this->taxaDeAprendizado * erroAux);
    }

    camadaAtual = camadaAtual - 1;
    //Camadas Intermedi�rias e camada de entrada
    for(camadaAtual = camadaAtual; camadaAtual > 0; camadaAtual--)
    {
        for(int i = 0; i < this->numeroNeuroniosEscondidos; i++)
        {
            double erroAux = 0;
            int proximaCamada = camadaAtual + 1;
            for(int j = 0; j < this->camadas[proximaCamada]->numeroDeNeuronios; j++)
            {
                erroAux += this->camadas[proximaCamada]->errosAux[j] * this->camadas[proximaCamada]->pesos[j][i];
            }

            erroAux = erroAux * this->camadas[camadaAtual]->valoresDeAtivacao[i];
            this->camadas[camadaAtual]->errosAux[i] = erroAux;

            int camadaAnterior = camadaAtual - 1;
            for(int j = 0; j < this->camadas[camadaAtual]->numeroDeNeuroniosCamadaAnterior; j++)
            {
                this->camadas[camadaAtual]->pesos[i][j] = this->camadas[camadaAtual]->pesos[i][j] +
                                                         (this->taxaDeAprendizado * erroAux * this->camadas[camadaAnterior]->valoresDeAtivacao[j]);
            }

            this->camadas[camadaAtual]->bias[i] = this->camadas[camadaAtual]->bias[i] + (this->taxaDeAprendizado * erroAux);
        }
    }
}

int * RNAConfig::obterValorEsperadoBinario(double valorEsperado)
{
    if(valorEsperado == 0)
    {
        return new int[3]{1, 0, 0};
    }
    else if(valorEsperado == 1)
    {
        return new int[3]{0, 1, 0};
    }
    else if (valorEsperado == 2)
    {
        return new int[3]{0, 0, 1};
    }
    else
    {
        return new int[3]{0, 0, 0};
    }
}

void RNAConfig::validar()
{
    ifstream file;
    file.open("validacao.txt");

    int quantidadeDeAcertos = 0;
    int quantidadeDeDadosDeteste = 0;
    int camadaDeSaida = this->numeroTotalDeCamadas - 1;

    for(int i = 0; i < this->numeroDeDadosDeValidacao; i++)
    {
        double valorEsperado = 0;
        file >> valorEsperado;

        for(int j = 0; j < this->numeroNosCamadaEntrada; j++)
        {
            file >> this->camadas[0]->valoresDeAtivacao[j];
        }

        if(valorEsperado == 0 || valorEsperado == 1 || valorEsperado == 2)
        {
            quantidadeDeDadosDeteste += 1;
            feedForward();

            int * valorEsperadoConvertido = obterValorEsperadoBinario(valorEsperado);
            if(determinarAcertoDaRNA(valorEsperadoConvertido, this->camadas[camadaDeSaida]->valoresDeAtivacao,
                                     this->camadas[camadaDeSaida]->numeroDeNeuronios))
            {
                quantidadeDeAcertos += 1;
            }
        }
    }

    double porcentagemDeSucesso = ((double)quantidadeDeAcertos / (double)quantidadeDeDadosDeteste) * 100;
    cout<<endl<<"O algoritmo acertou: "<<porcentagemDeSucesso<<" % dos dados de treinamento!"<<endl;

    file.close();
}

bool RNAConfig::determinarAcertoDaRNA(int * valorEsperado, double * valorRealObtido, int numeroDeNeuronios)
{
    bool retorno = true;

    for(int i = 0; i < numeroDeNeuronios; i++)
    {
        double diferenca = (double)valorEsperado[i] - valorRealObtido[i];
        //cout<<"Diferenca "<<diferenca<<endl;
        if(diferenca > 0.3 || diferenca < 0.3)
        {
            retorno = false;
        }
        cout<<valorEsperado[i]<<" "<<valorRealObtido[i]<<" --- ";
    }
    cout<<"   RET: "<<retorno<<endl;
    //cout<<endl;

    return retorno;
}
