#include "KMeans.h"

KMeans::KMeans(double ** valoresDeEntrada, int numeroDeGrupos, int numeroDeDimensoes, int numeroDeExemplos)
{
    this->valoresDeEntrada = valoresDeEntrada;
    this->numeroDeGrupos = numeroDeGrupos;
    this->numeroDeDimensoes = numeroDeDimensoes;
    this->numeroDeExemplos = numeroDeExemplos;

    this->inicializarCentroides();

    this->grupoDoExemplo = new double[numeroDeExemplos];
    for(int i = 0; i < 1000; i++)
    {
        this->determinarGrupoDoExemplo();
    }

    double numeroPorGrupo [this->numeroDeGrupos];
    for(int i = 0; i < this->numeroDeGrupos; i++)
    {
       numeroPorGrupo[i] = 0;
    }

    for(int i = 0; i < this->numeroDeExemplos; i++)
    {
        //cout<<"Para o exemplo: "<<i+1<<" tem-se o grupo: "<<this->grupoDoExemplo[i]<<endl;
        int aux = this->grupoDoExemplo[i];
        numeroPorGrupo[aux] += 1;
    }

    cout<<endl<<endl<<"Numero de exemplos por grupo!!!"<<endl;
    for(int i = 0; i < this->numeroDeGrupos; i++)
    {
        cout<<numeroPorGrupo[i]<<endl;
    }

    cout<<endl<<endl<<"Centroides:"<<endl;
    for(int i = 0; i < numeroDeGrupos; i++)
    {
        for(int j = 0; j < numeroDeDimensoes; j++)
        {
            cout<<this->centroides[i][j]<<" ";
        }
        cout<<endl;
    }
}

void KMeans::inicializarCentroides()
{
    this->centroides = new double*[this->numeroDeGrupos];
    for(int i = 0; i < this->numeroDeGrupos; i++)
    {
        int random = rand() % this->numeroDeExemplos;
        this->centroides[i] = new double[this->numeroDeDimensoes];

        for(int j = 0; j < this->numeroDeDimensoes; j++)
        {
            //Inicializar as centroides para cada grupo e dimens�o randomicamente.
            this->centroides[i][j] = this->valoresDeEntrada[random][j];
        }

        /*this->centroides[i][0] = this->valoresDeEntrada[i][0];
        this->centroides[i][1] = this->valoresDeEntrada[i][1];*/
    }

    /*for(int i = 0; i < numeroDeGrupos; i++)
    {
        for(int j = 0; j < numeroDeDimensoes; j++)
        {
            cout<<this->centroides[i][j]<<" ";
        }
        cout<<endl;
    }*/
}

void KMeans::determinarGrupoDoExemplo()
{
    for(int i = 0; i < this->numeroDeExemplos; i++)
    {
        int menorGrupo = 0;
        double menorDistancia = this->VALORMAXIMODOUBLE;
        for(int j = 0; j < this->numeroDeGrupos; j++)
        {
            double distanciaAux = 0;
            for(int a = 0; a < this->numeroDeDimensoes; a++)
            {
                distanciaAux += pow(this->valoresDeEntrada[i][a] - this->centroides[j][a], 2);
            }

            distanciaAux = sqrt(distanciaAux);

            //cout<<distanciaAux<<" ";
            if(distanciaAux < menorDistancia)
            {
                menorDistancia = distanciaAux;
                menorGrupo = j;
            }
        }
        //cout<<endl;

        this->grupoDoExemplo[i] = menorGrupo;
        //atualizarCentroide(i, menorGrupo);
    }

    /*for(int i = 0; i  < this->numeroDeExemplos; i++)
    {
        cout<<this->grupoDoExemplo[i]<<endl;
    }*/

    for(int i = 0; i < this->numeroDeGrupos; i++)
    {
        atualizarCentroide(0, i);
    }
}

void KMeans::atualizarCentroide(int posicaoDaEntrada, int grupoParaAtualizarCentroide)
{
    int quantidadeDeElementos = 0;

    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        this->centroides[grupoParaAtualizarCentroide][i] = 0;
    }

    for(int i = 0; i < this->numeroDeExemplos; i++)
    {
        if(this->grupoDoExemplo[i] == grupoParaAtualizarCentroide)
        {
            for(int j = 0; j < this->numeroDeDimensoes; j++)
            {
                this->centroides[grupoParaAtualizarCentroide][j] += this->valoresDeEntrada[i][j];
            }
            quantidadeDeElementos += 1;
        }
    }

    for(int i = 0; i < this->numeroDeDimensoes; i++)
    {
        this->centroides[grupoParaAtualizarCentroide][i] = this->centroides[grupoParaAtualizarCentroide][i] / quantidadeDeElementos;
    }
}

