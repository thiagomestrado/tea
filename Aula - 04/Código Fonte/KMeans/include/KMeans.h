#ifndef KMEANS_H
#define KMEANS_H

#include <stdlib.h>
#include <iostream>
#include <cmath>
#include <limits>
using namespace std;

class KMeans
{
    public:
        KMeans(double ** valoresDeEntrada, int numeroDeGrupos, int numeroDeDimensoes, int numeroDeExemplos);
        void inicializarCentroides();
        void determinarGrupoDoExemplo();
        void atualizarCentroide(int posicaoDaEntrada, int grupoParaAtualizarCentroide);

    protected:

    private:
        //Valores iniciais, passados para iniciar o algoritmo.
        double ** valoresDeEntrada;
        int numeroDeGrupos;
        int numeroDeDimensoes;
        int numeroDeExemplos;

        double ** centroides;
        double * grupoDoExemplo;
        const double VALORMAXIMODOUBLE = std::numeric_limits<double>::max();
};

#endif // KMEANS_H
