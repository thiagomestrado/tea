#include <iostream>
#include <fstream> //Para ler o arquivo .dat
#include <string>
#include "KMeans.h"

using namespace std;

int main()
{
    int numeroDeGrupos, numeroDeDimensoes, quantidadeDeExemplos;
    //string nomeDoArquivo = "Exercicio01.dat";
    //string nomeDoArquivo = "HTRU.dat";
    //string nomeDoArquivo = "Seeds.dat";
    string nomeDoArquivo = "iris.dat";

    cout<<"Digite o numero de grupos: "<<endl;
    cin >> numeroDeGrupos;

    cout<<"Digite o numero de Dimensoes: "<<endl;
    cin >> numeroDeDimensoes;

    cout<<"Digite a quantidade de Exemplos: "<<endl;
    cin >> quantidadeDeExemplos;

    //Exerc�cio 01
    //quantidadeDeExemplos = 17898;
    /*quantidadeDeExemplos = 150;
    numeroDeDimensoes = 2;
    numeroDeGrupos = 3;*/

    //Obtendo os dados do arquivo .dat
    std::ifstream file;
    file.open(nomeDoArquivo);
    double ** valoresDeEntrada = new double*[quantidadeDeExemplos];
    double fora = 0;
    for(int i = 0; i < quantidadeDeExemplos; i++)
    {
        //file >> fora;
        //file >> fora;

        valoresDeEntrada[i] = new double[numeroDeDimensoes];
        for(int j = 0; j < numeroDeDimensoes; j++)
        {
            file >> valoresDeEntrada[i][j];
        }

        file >> fora;
    }
    file.close();

    try
    {
        KMeans kmeans(valoresDeEntrada, numeroDeGrupos, numeroDeDimensoes, quantidadeDeExemplos);
    }
    catch(const char* mensagemDeErro)
    {
        cout<<"Ocorreu um erro ao tentar classificar os exemplos: "<<mensagemDeErro<<endl;
    }

    /*for(int i = 0; i < quantidadeDeExemplos; i++)
    {
        for(int j = 0; j < numeroDeDimensoes; j++)
        {
            cout<<valoresDeEntrada[i][j]<<" ";
        }
        cout<<endl;
    }*/

    return 0;
}
