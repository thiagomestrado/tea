#include <iostream>
#include "GridWorld.h"
#include "MonteCarlo.h"

using namespace std;

int main()
{
    try
    {
        int quantidadeDeEspisodios = 1000000;
        cout<<"Iniciando Monte Carlo, com "<<quantidadeDeEspisodios<<" episodios!!!"<<endl<<endl;

        GridWorld * mundoDeBlocos = new GridWorld();
        new MonteCarlo(mundoDeBlocos, quantidadeDeEspisodios);
    }
    catch(const char* mensagemDeErro)
    {
        cout<<"Ocorreu um erro ao tentar utilizar Monte Carlo: "<<mensagemDeErro<<endl;
    }

    return 0;
}
