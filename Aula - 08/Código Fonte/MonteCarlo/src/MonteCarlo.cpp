#include "MonteCarlo.h"

MonteCarlo::MonteCarlo(GridWorld * mundoDeBlocos, int quantidadeDeEpisodios)
{
    srand(time(NULL));

    this->mundoDeBlocos = mundoDeBlocos;
    this->quantidadeDeEpisodios = quantidadeDeEpisodios;

    this->inicializarMediaDosEstados();

    this->avaliacaoDaPolitica();
}

void MonteCarlo::avaliacaoDaPolitica()
{
    //Loop principal do algoritmo de Monte Carlo, vai gerar essa quantidade de epis�dios e ent�o retornar uma pol�tica
    for(int i = 0; i < this->quantidadeDeEpisodios; i++)
    {
        vector<Estado> * estados = new vector<Estado>();
        gerarEpisodio(estados);

        //Agora fazer a avalia��o para cada um dos estados que est� no epis�dio.

        vector<Estado> estadosJaVisitados;
        vector<Estado>::iterator it = estadosJaVisitados.begin();

        for(int j = 0; j < estados->size(); j++)
        {
            Estado estadoAtual = estados->at(j);
            //bool estadoJaVisitado = this->verificarEstadoJaVisitado(estadosJaVisitados, estadoAtual);
            bool estadoJaVisitado = this->verificarEstadoAcaoJaVisitados(estadosJaVisitados, estadoAtual);

            if(!estadoJaVisitado)
            {
                estados->at(j).indexPrimeiraPosicao = j;

                /*double somaAux = estadoAtual.recompensa;
                for(int a = j+1; a < estados->size(); a++)
                {
                    somaAux += (estados->at(a).recompensa);
                }

                int posicaoHorizontal = estadoAtual.posicaoHorizontal;
                int posicaoVertical = estadoAtual.posicaoVertical;
                int acao = estadoAtual.acaoExecutada;

                this->recompensasSomadasPorAcao[posicaoHorizontal][posicaoVertical][acao] += somaAux;

                this->quantidadeDeAparicoesEstadoAcao[posicaoHorizontal][posicaoVertical][acao] += 1;

                this->mundoDeBlocos->valorPoliticaPorAcao[posicaoHorizontal][posicaoVertical][acao] =
                            this->recompensasSomadasPorAcao[posicaoHorizontal][posicaoVertical][acao] /
                            this->quantidadeDeAparicoesEstadoAcao[posicaoHorizontal][posicaoVertical][acao];*/

                this->atualizarValorQ(estados, estadoAtual, j);

                estadosJaVisitados.insert(it, estadoAtual);
                it = estadosJaVisitados.end();
            }
            else
            {
                int posicao = -1;
                for(int b = 0; b < estados->size() && posicao == -1; b++)
                {
                    if(this->verificarEstadoAcaoJaVisitados(estadosJaVisitados, estadoAtual))
                    {
                        posicao = b;
                    }
                }

                this->atualizarValorQ(estados, estadoAtual, posicao);
            }
        }


        vector<Estado> estadosJaVisitados2;
        it = estadosJaVisitados2.begin();
        for(int j = 0; j < estados->size(); j++)
        {
            Estado estadoAtual = estados->at(j);
            bool estadoJaVisitado = this->verificarEstadoJaVisitado(estadosJaVisitados2, estadoAtual);

            if(!estadoJaVisitado)
            {
                this->determinarMelhorPoliticaPorEstado(estadoAtual.posicaoHorizontal, estadoAtual.posicaoVertical);

                estadosJaVisitados2.insert(it, estadoAtual);
                it = estadosJaVisitados2.end();
            }
        }
    }

    this->determinarMelhorPolitica();
}

void MonteCarlo::determinarMelhorPolitica()
{
    double acerto = 0;

    for(int i = 0; i < this->mundoDeBlocos->tamanhoMundoHorizontal; i++)
    {
        for(int j = 0; j < this->mundoDeBlocos->tamanhoMundoVertical; j++)
        {
            double valorMelhorAcao = this->mundoDeBlocos->valorPoliticaPorAcao[i][j][0];
            double melhorAcao = 0;

            for(int a = 1; a < this->mundoDeBlocos->numeroDeAcoes; a++)
            {
                if(this->mundoDeBlocos->valorPoliticaPorAcao[i][j][a] > valorMelhorAcao)
                {
                    valorMelhorAcao = this->mundoDeBlocos->valorPoliticaPorAcao[i][j][a];
                    melhorAcao = a;
                }
            }

            this->mundoDeBlocos->politica[i][j] = melhorAcao;
            if((i != 0 || j != 0) && (i != 3 || j != 3))
            {
               cout<<"Estado: "<<i<<"  ---  "<<j<<"  e acao: "<<melhorAcao<<endl;
               /*bool correto = this->acertou(i, j, (int)melhorAcao);
               if(correto)
               {
                   //cout<<"  ----  "<<endl;
                   acerto = acerto + 1;
               }*/
            }
        }
    }

    //cout<<endl<<endl<<"A porcentagem de acerto eh: "<<((acerto/14 * 100));
}

void MonteCarlo::determinarMelhorPoliticaPorEstado(int posicaoHorizontal, int posicaoVertical)
{
    double valorMelhorAcao = this->mundoDeBlocos->valorPoliticaPorAcao[posicaoHorizontal][posicaoVertical][0];
    double melhorAcao = 0;

    for(int a = 1; a < this->mundoDeBlocos->numeroDeAcoes; a++)
    {
        if(this->mundoDeBlocos->valorPoliticaPorAcao[posicaoHorizontal][posicaoVertical][a] > valorMelhorAcao)
        {
            valorMelhorAcao = this->mundoDeBlocos->valorPoliticaPorAcao[posicaoHorizontal][posicaoVertical][a];
            melhorAcao = a;
        }
    }

    this->mundoDeBlocos->politica[posicaoHorizontal][posicaoVertical] = melhorAcao;
}

void MonteCarlo::gerarEpisodio(vector<Estado> * estados)
{
    bool estadoTerminal = false;
    int quantidadeMaximaDeEstados = 50;
    int contador = 0;
    vector<Estado>::iterator it;
    it = estados->begin();

    int posicaoHorizontalAtual = this->mundoDeBlocos->politicaRandomica();
    int posicaoVerticalAtual = this->mundoDeBlocos->politicaRandomica();

    estadoTerminal = this->mundoDeBlocos->verificarSeEstadoObjetivo(posicaoHorizontalAtual, posicaoVerticalAtual);
    if(estadoTerminal)
    {
        gerarEpisodio(estados);
        return;
    }

    while(!estadoTerminal && contador < quantidadeMaximaDeEstados)
    {
        //int acaoAExecutar = this->mundoDeBlocos->politicaRandomica();
        //int acaoAExecutar = this->mundoDeBlocos->politica[posicaoHorizontalAtual][posicaoVerticalAtual];
        int acaoAExecutar = this->obterAcao(posicaoHorizontalAtual, posicaoVerticalAtual);
        int * estadoFuturo = this->mundoDeBlocos->obterEstadoFuturo(posicaoHorizontalAtual, posicaoVerticalAtual, acaoAExecutar);
        double recompensaAux = this->mundoDeBlocos->funcaoRecompensa[posicaoHorizontalAtual][posicaoVerticalAtual][acaoAExecutar];

        Estado estadoAux(posicaoHorizontalAtual, posicaoVerticalAtual, recompensaAux, acaoAExecutar);

        estados->insert(it, estadoAux);

        estadoTerminal = this->mundoDeBlocos->verificarSeEstadoObjetivo(posicaoHorizontalAtual, posicaoVerticalAtual);
        contador++;

        posicaoHorizontalAtual = estadoFuturo[0];
        posicaoVerticalAtual = estadoFuturo[1];
        it = estados->end();
    }
}

bool MonteCarlo::verificarEstadoJaVisitado(vector<Estado> estadosJaVisitados, Estado estadoParaVerificar)
{
    for(int i = 0; i < estadosJaVisitados.size(); i++)
    {
        if(estadosJaVisitados.at(i).posicaoHorizontal == estadoParaVerificar.posicaoHorizontal &&
           estadosJaVisitados.at(i).posicaoVertical == estadoParaVerificar.posicaoVertical)
        {
            return true;
        }
    }

    return false;
}

bool MonteCarlo::verificarEstadoAcaoJaVisitados(vector<Estado> estadosJaVisitados, Estado estadoParaVerificar)
{
    for(int i = 0; i < estadosJaVisitados.size(); i++)
    {
        if(estadosJaVisitados.at(i).posicaoHorizontal == estadoParaVerificar.posicaoHorizontal &&
           estadosJaVisitados.at(i).posicaoVertical == estadoParaVerificar.posicaoVertical &&
           estadosJaVisitados.at(i).acaoExecutada == estadoParaVerificar.acaoExecutada)
        {
            return true;
        }
    }

    return false;
}

void MonteCarlo::inicializarMediaDosEstados()
{
    for(int i = 0; i < this->mundoDeBlocos->tamanhoMundoHorizontal; i++)
    {
        for(int j = 0; j < this->mundoDeBlocos->tamanhoMundoVertical; j++)
        {
            this->recompensasSomadas[i][j] = 0;
            this->quantidadeDeAparicoesEstado[i][j] = 0;
            for(int a = 0; a < this->mundoDeBlocos->numeroDeAcoes; a++)
            {
                this->quantidadeDeAparicoesEstadoAcao[i][j][a] = 0;
                this->recompensasSomadasPorAcao[i][j][a] = 0;
            }
        }
    }
}

int MonteCarlo::obterAcao(int posicaoHorizontal, int posicaoVertical)
{
    //10% dos casos vai ser rand�mico.
    double limite = 10;

    int valor = (rand() % 101);
    if(valor > limite)
    {
        return this->mundoDeBlocos->politica[posicaoHorizontal][posicaoVertical];
    }
    else
    {
        return this->mundoDeBlocos->politicaRandomica();
    }
}

void MonteCarlo::atualizarValorQ(vector<Estado> * estados, Estado estadoAtual, int indexPrimeiraOcorrencia)
{
    double somaAux = estadoAtual.recompensa;
    for(int a = indexPrimeiraOcorrencia + 1; a < estados->size(); a++)
    {
        somaAux += (estados->at(a).recompensa);
    }

    int posicaoHorizontal = estadoAtual.posicaoHorizontal;
    int posicaoVertical = estadoAtual.posicaoVertical;
    int acao = estadoAtual.acaoExecutada;

    this->recompensasSomadasPorAcao[posicaoHorizontal][posicaoVertical][acao] += somaAux;

    this->quantidadeDeAparicoesEstadoAcao[posicaoHorizontal][posicaoVertical][acao] += 1;

    this->mundoDeBlocos->valorPoliticaPorAcao[posicaoHorizontal][posicaoVertical][acao] =
                this->recompensasSomadasPorAcao[posicaoHorizontal][posicaoVertical][acao] /
                this->quantidadeDeAparicoesEstadoAcao[posicaoHorizontal][posicaoVertical][acao];
}

/*bool MonteCarlo::acertou(int posicaoHorizontal, int posicaoVertical, int acao)
{
    if(posicaoHorizontal == 0 && posicaoVertical == 1)
    {
        if(acao == 3)
            return true;
    }
    else if(posicaoHorizontal == 0 && posicaoVertical == 2)
    {
        if(acao == 3)
            return true;
    }
    else if(posicaoHorizontal == 0 && posicaoVertical == 3)
    {
        if(acao == 2 || acao == 3)
            return true;
    }
    else if(posicaoHorizontal == 1 && posicaoVertical == 0)
    {
        if(acao == 0)
            return true;
    }
    else if(posicaoHorizontal == 1 && posicaoVertical == 1)
    {
        if(acao == 0 || acao == 3)
            return true;
    }
    else if(posicaoHorizontal == 1 && posicaoVertical == 2)
    {
        if(acao == 2 || acao == 3)
            return true;
    }
    else if(posicaoHorizontal == 1 && posicaoVertical == 3)
    {
        if(acao == 2)
            return true;
    }
    else if(posicaoHorizontal == 2 && posicaoVertical == 0)
    {
        if(acao == 0)
            return true;
    }
    else if(posicaoHorizontal == 2 && posicaoVertical == 1)
    {
        if(acao == 0 || acao == 1)
            return true;
    }
    else if(posicaoHorizontal == 2 && posicaoVertical == 2)
    {
        if(acao == 1 || acao == 2)
            return true;
    }
    else if(posicaoHorizontal == 2 && posicaoVertical == 3)
    {
        if(acao == 2)
            return true;
    }
    else if(posicaoHorizontal == 3 && posicaoVertical == 0)
    {
        if(acao == 0 || acao == 1)
            return true;
    }
        else if(posicaoHorizontal == 3 && posicaoVertical == 1)
    {
        if(acao == 1)
            return true;
    }
    else
    {
        if(acao == 1)
            return true;
    }

    return false;
}*/


