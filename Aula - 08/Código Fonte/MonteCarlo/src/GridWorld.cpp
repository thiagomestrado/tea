#include "GridWorld.h"

GridWorld::GridWorld()
{
    srand (time(NULL));

    this->inicializarFuncaoValor();
    this->inicializarFuncaoRecompensa();
    this->inicializarFuncaoTransicao();
    this->inicializarPoliticaPorAcao();
}

int GridWorld::politicaRandomica()
{
    ///Retorna uma a��o rand�mica para aquele estado.
    return (rand() % 4);
}

void GridWorld::inicializarFuncaoValor()
{
    for(int i = 0; i < this->tamanhoMundoHorizontal; i++)
    {
        for(int j = 0; j < this->tamanhoMundoVertical; j++)
        {
            this->funcaoValor[i][j] = 0;
        }
    }
}

void GridWorld::inicializarFuncaoRecompensa()
{
    for(int i = 0; i < this->tamanhoMundoHorizontal; i++)
    {
        for(int j = 0; j < this->tamanhoMundoVertical; j++)
        {
            for(int a = 0; a < this->numeroDeAcoes; a++)
            {
                int * estadosFuturos = obterEstadoFuturo(i, j, a);
                int estadoEmInteiro = this->converterDeEstadoParaInteiro(estadosFuturos[0], estadosFuturos[1]);

                if(estadoEmInteiro == 0 || estadoEmInteiro == 15)
                {
                    this->funcaoRecompensa[i][j][a] = this->recompensaEstadoObjetivo;
                }
                else
                {
                    this->funcaoRecompensa[i][j][a] = this->recompensaPorPasso;
                }
            }
        }
    }
}


void GridWorld::inicializarFuncaoTransicao()
{
    for(int i = 0; i < this->tamanhoMundoHorizontal; i++)
    {
        for(int j = 0; j < this->tamanhoMundoVertical; j++)
        {
            for(int a = 0; a < this->numeroDeAcoes; a++)
            {
                //Iniciar todos para zero para facilitar na conta da m�dia.
                this->valorFuncaoTransicao[i][j][a] = 0;
            }
        }
    }
}

void GridWorld::inicializarPoliticaPorAcao()
{
    for(int i = 0; i < this->tamanhoMundoHorizontal; i++)
    {
        for(int j = 0; j < this->tamanhoMundoVertical; j++)
        {
            //Politica inicial � rand�mica
            this->politica[i][j] = this->politicaRandomica();

            for(int a = 0; a < this->numeroDeAcoes; a++)
            {
                this->valorPoliticaPorAcao[i][j][a] = 0;
            }
        }
    }
}

int * GridWorld::obterEstadoFuturo(int posicaoHorizontal, int posicaoVertical, int acao)
{
    if(acao == 0) //Para Cima
    {
        if(posicaoHorizontal > 0)
            posicaoHorizontal -= 1;
    }
    else if(acao == 1) //Para Direita
    {
        if(posicaoVertical < 3)
            posicaoVertical += 1;
    }
    else if(acao == 2) //Para Baixo
    {
        if(posicaoHorizontal < 3)
            posicaoHorizontal += 1;
    }
    else //Para Esquerda
    {
        if(posicaoVertical > 0)
            posicaoVertical -= 1;
    }

    int estadoFuturo[2] = {posicaoHorizontal, posicaoVertical};
    return estadoFuturo;
}

int GridWorld::converterDeEstadoParaInteiro(int posicaoHorizontal, int posicaoVertical)
{
    return (posicaoHorizontal * this->numeroDeAcoes + posicaoVertical);
}

int GridWorld::obterAcaoComMaiorValorDePolitica(int posicaoHorizontal, int posicaoVertical)
{
    double maiorValor = -1000000;
    int melhorAcao = 0;

    for(int a = 0; a < this->numeroDeAcoes; a++)
    {
        if(this->valorPoliticaPorAcao[posicaoHorizontal][posicaoVertical][a] > maiorValor)
        {
            maiorValor = this->valorPoliticaPorAcao[posicaoHorizontal][posicaoVertical][a];
            melhorAcao = a;
        }
    }

    return melhorAcao;
}

bool GridWorld::verificarSeEstadoObjetivo(int posicaoHorizontal, int posicaoVertical)
{
    if((posicaoHorizontal == 0 && posicaoVertical == 0) ||
       (posicaoHorizontal == 3 && posicaoVertical == 3))
    {
        return true;
    }

    return false;
}
