#ifndef MONTECARLO_H
#define MONTECARLO_H

#include <stdlib.h>
#include <time.h>
#include <vector>
#include "GridWorld.h"
#include "Estado.h"

using namespace std;

class MonteCarlo
{
    public:
        MonteCarlo(GridWorld * mundoDeBlocos, int quantidadeDeEpisodios);
        void avaliacaoDaPolitica();
        void determinarMelhorPolitica();
        void determinarMelhorPoliticaPorEstado(int posicaoHorizontal, int posicaoVertical);
        double obterAbsoluto(double primeiraParte, double segundaParte);
        void gerarEpisodio(vector<Estado> * estados);
        void inicializarMediaDosEstados();
        bool verificarEstadoJaVisitado(vector<Estado> estadosJaVisitados, Estado estadoParaVerificar);
        bool verificarEstadoAcaoJaVisitados(vector<Estado> estadosJaVisitados, Estado estadoParaVerificar);
        int obterAcao(int posicaoHorizontal, int posicaoVertical);
        void atualizarValorQ(vector<Estado> * estados, Estado estadoAtual, int indexPrimeiraOcorrencia);
        bool acertou(int posicaoHorizontal, int posicaoVertical, int acao);

    protected:

    private:
        GridWorld * mundoDeBlocos;
        int quantidadeDeEpisodios;
        double recompensasSomadas[4][4]; ///Recompensa para um determinado estado somadas. Usado por MC
        int quantidadeDeAparicoesEstado[4][4]; ///Quantidade de vezes que o estado apareceu no epis�dio. Usado por MC
        int quantidadeDeAparicoesEstadoAcao[4][4][4];
        double recompensasSomadasPorAcao[4][4][4]; ///REcompensas por estado e a��o. Cont�m a somat�ria que ser� usada na m�dia.
};

#endif // MONTECARLO_H
