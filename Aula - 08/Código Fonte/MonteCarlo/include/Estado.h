#ifndef ESTADO_H
#define ESTADO_H


class Estado
{
    public:
        Estado(int posicaoHorizontal, int posicaoVertical, double recompensa, int acaoExecutada);
        int posicaoHorizontal;
        int posicaoVertical;
        double recompensa;
        int acaoExecutada;
        int indexPrimeiraPosicao;

    protected:

    private:
};

#endif // ESTADO_H
