#ifndef GRIDWORLD_H
#define GRIDWORLD_H

#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

class GridWorld
{
    public:
        ///M�todos
        GridWorld();
        int politicaRandomica(); ///Retorna uma a��o a ser executada no ambiente
        void inicializarFuncaoValor();
        void inicializarFuncaoRecompensa();
        void inicializarFuncaoTransicao();
        void inicializarPoliticaPorAcao();
        int * obterEstadoFuturo(int posicaoHorizontal, int posicaoVertical, int acao);
        int converterDeEstadoParaInteiro(int posicaoHorizontal, int posicaoVertical);
        int obterAcaoComMaiorValorDePolitica(int posicaoHorizontal, int posicaoVertical);
        bool verificarSeEstadoObjetivo(int posicaoHorizontal, int posicaoVertical);

        ///Atributos
        int tamanhoMundoHorizontal = 4;
        int tamanhoMundoVertical = 4;
        int numeroDeAcoes = 4;
        double recompensaPorPasso = -1.0;
        double recompensaEstadoObjetivo = 0.0;
        double funcaoValor[4][4]; //Uma valor para cada estado.
        double funcaoRecompensa[4][4][4]; ///Recompensa para aquele estado quando realiza uma das 4 a��es.
        double funcaoTransicao[4][4][4]; ///Para onde vai, dado cada estado e a��o poss�vel. N�o usado por Monte Carlo
        double valorFuncaoTransicao[4][4][4]; ///N�o usado por Monte Carlo
        double politica[4][4]; ///Uma a��o para cada estado poss�vel. Essa matriz deve conter a a��o �tima para cada estado.
        double valorPoliticaPorAcao[4][4][4]; ///Um valor de pol�tica para cada a��o.
        double probabilidadeDeUmaAcaoSerExecutada = 0.25;

        ///A��es poss�veis de serem executadas no ambiente
        int paraCima = 0;
        int paraDireita = 1;
        int paraBaixo = 2;
        int paraEsquerda = 3;

    protected:

    private:
};

#endif // GRIDWORLD_H
